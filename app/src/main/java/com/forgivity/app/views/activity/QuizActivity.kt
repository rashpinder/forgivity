package com.forgivity.app.views.activity

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.adapter.RelatedQuizAdapter
import com.forgivity.app.interfaces.OnQuizItemClickInterface
import com.forgivity.app.model.*
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.util.AppPreference
import com.forgivity.app.util.SUBMIT_ANS_DONE
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_my_program.*
import kotlinx.android.synthetic.main.activity_quiz.*
import kotlinx.android.synthetic.main.activity_review.*
import kotlinx.android.synthetic.main.activity_review.optionsRV
import kotlinx.android.synthetic.main.activity_review.txtQuesTV
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class QuizActivity : BaseActivity() {
    var mQuizQuesList: ArrayList<RelatedQues> = ArrayList()
    var mRelatedQuiz: RelatedQuiz? = null
    var mRelatedQues: RelatedQues? = null
    var mNug_id = ""
    var quesId = ""
    var optionKey = ""
    var nugID = ""
    var ansDesc = ""
    var correctOption = ""
    var mPos: Int = 0
    lateinit var mRelatedQuizAdapter: RelatedQuizAdapter
    var mOptionsList: ArrayList<Options> = ArrayList()
    var mNuggetsList: ArrayList<RelatedNuggetsItem> = ArrayList()
    var mNuggetsReadOutList: ArrayList<String> = ArrayList()
    var position: Int = 0
    var nug_id: String? = ""
    var quizId: String? = ""
    var mQuizReadOutList: ArrayList<String> = ArrayList()
    var mAllQuizQuesList: ArrayList<RelatedQues> = ArrayList()
    var mRelatedPod: RelatedPod? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)
        ButterKnife.bind(this)
        getIntentData()
    }


    override fun onResume() {
        super.onResume()
//        if (mQuizQuesList[mPos].answerStatus!!.equals(false)){
        txtQuesTV.text = mQuizQuesList.get(mPos).quesDesc

        quesNumTV.text = "Question " + (mPos + 1) + " of " + mQuizQuesList.size

        mOptionsList.clear()
        for (i in mQuizQuesList.indices) {
            val hashMap = HashMap<String, String>()
            val getResultCon = mQuizQuesList[i].options
            val gson = Gson()
            val jsonElement = gson.toJsonTree(getResultCon)
            var mLiiist = gson.fromJson(jsonElement, hashMap::class.java)

            var index = 0
            for ((key, value) in mLiiist) {
                var keyByIndex =
                    mLiiist.keys.elementAt(index) // Get key by index.
                val valueOfElement =
                    mLiiist.getValue(keyByIndex) // Get value.

                var mModel: Options? = Options()
                mModel?.option_key = keyByIndex
                mModel?.option_value = valueOfElement
                mOptionsList.add(mModel!!)
                index++
            }
//            mQuizQuesList[i].answerStatus=true
            mPos = i
            break
        }
        setQuizAdapter()
//    }
    }

    private fun getIntentData() {
        if (intent != null) {
            if (mQuizQuesList != null) {
                mQuizQuesList =
                    intent.getParcelableArrayListExtra<RelatedQues>("quiz_questions_list")!!
            }
                mRelatedPod = intent.getParcelableExtra("related_pod_list")
            mNug_id = intent.getStringExtra("nug_id").toString()
            Log.e(TAG, "list" + mQuizQuesList)
        }
        Log.e(TAG, "********optionslist*********" + mOptionsList)
    }

    private fun setQuizAdapter() {
        mRelatedQuizAdapter = RelatedQuizAdapter(
            mActivity,
            mQuizQuesList[mPos].answerDesc.toString(),
            mQuizQuesList[mPos].correctOption.toString(),
            mOptionsList,
            mItemClickListner,
            mNug_id,
            mQuizQuesList[mPos].quesId.toString()
        )
        optionsRV.layoutManager = LinearLayoutManager(mActivity)
        optionsRV.adapter = mRelatedQuizAdapter
//        mRelatedQuizAdapter.notifyDataSetChanged()
    }

    @OnClick(
        R.id.imgBackIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> performBackClick()
        }
    }

    private fun performBackClick() {
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    //click of options
    var mItemClickListner: OnQuizItemClickInterface = object : OnQuizItemClickInterface {
        override fun onQuizItemClickListner(
            quesId: String,
            optionKey: String,
            nugID: String,
            ansDesc: String?,
            correctOption: String?
        ) {
            this@QuizActivity.quesId = quesId
            this@QuizActivity.optionKey = optionKey
            this@QuizActivity.nugID = nugID
            this@QuizActivity.ansDesc = ansDesc.toString()
            this@QuizActivity.correctOption = correctOption.toString()
            submitQuestionApi()
        }
    }

    /*
    submit selected answer api
     */
    private fun submitQuestionApi() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeSubmitAnswerRequest()
        }
    }

    private fun executeSubmitAnswerRequest() {
        showProgressDialog(mActivity)
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.submitAnswerRequest(getAccessToken(), quesId, optionKey, nugID)
            .enqueue(object :
                Callback<SubmitAnswerModel?> {
                override fun onResponse(
                    call: Call<SubmitAnswerModel?>?,
                    response: Response<SubmitAnswerModel?>
                ) {
                    dismissProgressDialog()
                    val mModel: SubmitAnswerModel? = response.body()
                    if (mModel?.status == 1) {
//                        showToast(mActivity, "Submitted")

//                       to Show Correct/Incorrect Popup
                        Log.e(TAG, "Option_key" + optionKey)
                        Log.e(TAG, "Option_key" + correctOption)

                        if (optionKey.equals(correctOption)) {
                            showCorrectAlertDialog(mActivity, ansDesc,mRelatedPod)
                        } else {
                            showIncorrectAlertDialog(mActivity, ansDesc,mRelatedPod)
                        }
                    } else {
                        dismissProgressDialog()
                        showToast(mActivity, mModel?.message)
                    }
                }

                override fun onFailure(call: Call<SubmitAnswerModel?>?, t: Throwable?) {
                    dismissProgressDialog()
                    Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
                }
            })
    }


    private fun getReadingDetails() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeApi()
        }
    }

    private fun executeApi() {
        mNuggetsList.clear()
        mNuggetsReadOutList.clear()
        showProgressDialog(mActivity)
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getReadingRequest(getAccessToken()).enqueue(object :
            Callback<ReadingModel?> {
            override fun onResponse(
                call: Call<ReadingModel?>?,
                response: Response<ReadingModel?>
            ) {
                dismissProgressDialog()
                val mModel: ReadingModel? = response.body()
                if (mModel?.status == 1) {
                    mNuggetsList = mModel.dayDetail!!.relatedNuggets

                    for (i in mNuggetsList.indices) {
                        if (mNuggetsList.get(i).readStatus == 1) {
                            mNuggetsReadOutList.add(mNuggetsList.get(i).toString())
                        }

                        if (mNuggetsList.get(i).readStatus == 0) {
                            nug_id = mNuggetsList.get(i).nugId
                            position = i
                            Log.e(TAG, "position" + position)
                        }


                        if (mModel.dayDetail.relatedNuggets[i].relatedQues is Any) {
                            if (mModel.dayDetail.relatedNuggets[i].relatedQues == "0") {
                                Log.e(TAG, "relatedQues 0")
                            } else {
                                val getResultCon =
                                    mModel.dayDetail.relatedNuggets[i].relatedQues // response Any
                                val gson = Gson()
                                val jsonElement = gson.toJsonTree(getResultCon)
                                mRelatedQues = gson.fromJson(jsonElement, RelatedQues::class.java)
                                Log.e(TAG, "relatedQues" + mRelatedQues)
                            }
                        }

                        //getting related quiz in nuggets
                        if (mModel.dayDetail.relatedNuggets[i].relatedQuiz is Any) {
                            if (mModel.dayDetail.relatedNuggets[i].relatedQuiz == "0") {
                            } else {
                                if (mNuggetsList.get(position).relatedQuiz != "0") {
                                    if (mNuggetsList.get(position).relatedQuiz is Any) {
                                        val getResultCon =
                                            mNuggetsList.get(position).relatedQuiz
                                        val gson = Gson()
                                        val jsonElement = gson.toJsonTree(getResultCon)
                                        mRelatedQuiz =
                                            gson.fromJson(jsonElement, RelatedQuiz::class.java)
                                        quizId= mRelatedQuiz?.quizId
                                        if (mRelatedQuiz?.questions != null) {
                                            var mQuizPendingList: ArrayList<RelatedQues> =
                                                ArrayList()

                                            mQuizQuesList.clear()
                                            mAllQuizQuesList.clear()
                                            mQuizReadOutList.clear()

                                            mAllQuizQuesList =
                                                mRelatedQuiz?.questions as ArrayList<RelatedQues>

                                            for (j in mAllQuizQuesList.indices) {

                                                if (mAllQuizQuesList[j].answerStatus!!.equals(true)) {
                                                    mQuizReadOutList.add(
                                                        mAllQuizQuesList.get(j).toString()
                                                    )
                                                    if (mAllQuizQuesList.size.equals(
                                                            mQuizReadOutList.size
                                                        )
                                                    ) {
                                                        AppPreference().writeString(
                                                            mActivity,
                                                            SUBMIT_ANS_DONE,
                                                            "done"
                                                        )
                                                        val i = Intent(mActivity, ResultActivity::class.java)
                                                        i.putExtra("quiz_id", quizId)
                                                        startActivity(i)
//                                                        showToast(mActivity, "All questions Answered")
                                                        finish()
                                                    }
                                                } else if (mAllQuizQuesList[j].answerStatus!!.equals(
                                                        false
                                                    )
                                                ) {
                                                    mQuizQuesList.add(mAllQuizQuesList.get(j))
                                                    txtQuesTV.text =
                                                        mAllQuizQuesList.get(j).quesDesc
                                                    quesNumTV.text =
                                                        "Question " + (j + 1) + " of " + mAllQuizQuesList.size
                                                    mOptionsList.clear()
                                                    for (k in mQuizQuesList.indices) {
                                                        val hashMap = HashMap<String, String>()
                                                        val getResultCon = mQuizQuesList[k].options
                                                        val gson = Gson()
                                                        val jsonElement =
                                                            gson.toJsonTree(getResultCon)
                                                        var mLiiist = gson.fromJson(
                                                            jsonElement,
                                                            hashMap::class.java
                                                        )

                                                        var index = 0
                                                        for ((key, value) in mLiiist) {
                                                            var keyByIndex =
                                                                mLiiist.keys.elementAt(index) // Get key by index.
                                                            val valueOfElement =
                                                                mLiiist.getValue(keyByIndex) // Get value.

                                                            var mModel: Options? = Options()
                                                            mModel?.option_key = keyByIndex
                                                            mModel?.option_value = valueOfElement
                                                            mOptionsList.add(mModel!!)
                                                            index++
                                                        }
//            mQuizQuesList[i].answerStatus=true
                                                        mPos = k
                                                        break

                                                    }
                                                    setQuizAdapter()
                                                    break
                                                }
                                            }

                                        }
                                    }

                                    //getting related pods
                                    if (mModel.dayDetail.relatedPod is Any) {
                                        if (mModel.dayDetail.relatedPod == "0") {

                                        } else {
                                            val getResultCon = mModel.dayDetail.relatedPod
                                            val gson = Gson()
                                            val jsonElement = gson.toJsonTree(getResultCon)
                                            val mRelatedPod =
                                                gson.fromJson(jsonElement, RelatedPod::class.java)

                                        }
                                    } else {
                                        Log.e("GetReadingViewModel", "******ERROR******")
                                    }
                                } else {
                                    dismissProgressDialog()
                                    showToast(mActivity, mModel.message)
                                }
                            }
                        }
                    }
                }
            }

            override fun onFailure(call: Call<ReadingModel?>?, t: Throwable?) {
                dismissProgressDialog()
                Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
            }
        })
    }


    /*
     *
     * Incorrect Answer Alert Dialog
     * */
    fun showIncorrectAlertDialog(mActivity: Activity?, strMessage: String?,mRelatedPod: RelatedPod?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_incorrect)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDoneIncorrect = alertDialog.findViewById<TextView>(R.id.btnDoneIncorrect)
        val txtRelatedListeningTV = alertDialog.findViewById<TextView>(R.id.txtRelatedListeningTV)
        txtMessageTV.text = strMessage
        btnDoneIncorrect.setOnClickListener {
            alertDialog.dismiss()
            getReadingDetails()
//            AppPreference().writeString(mActivity,SUBMIT_ANS_DONE,"done")
//            finish()
        }
        txtRelatedListeningTV.setOnClickListener {
//            alertDialog.dismiss()
            val i = Intent(mActivity, AudiooActivity::class.java)
            i.putExtra("list", mRelatedPod)
            startActivity(i)
        }

        alertDialog.show()
    }

    /*
     *
     * Correct Answer Alert Dialog
     * */
    fun showCorrectAlertDialog(mActivity: Activity?, strMessage: String?,mRelatedPod: RelatedPod?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_correct)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val txtTitleTV = alertDialog.findViewById<TextView>(R.id.txtTitleTV)
        val btnDoneCorrect = alertDialog.findViewById<TextView>(R.id.btnDoneCorrect)
        val txtRelatedListeningTV = alertDialog.findViewById<TextView>(R.id.txtRelatedListeningTV)
        txtMessageTV.text = strMessage
//        txtTitleTV.text = strMessage
        btnDoneCorrect.setOnClickListener {
            alertDialog.dismiss()
            getReadingDetails()
//            AppPreference().writeString(mActivity,SUBMIT_ANS_DONE,"done")
//            finish()
        }
        txtRelatedListeningTV.setOnClickListener {
//            alertDialog.dismiss()
            val i = Intent(mActivity, AudiooActivity::class.java)
            i.putExtra("list", mRelatedPod)
            startActivity(i)
        }
        alertDialog.show()
    }


}
package com.forgivity.app.views.activity

import android.os.Bundle
import butterknife.ButterKnife
import com.forgivity.app.R
import com.forgivity.app.adapter.IntroAdapter
import com.forgivity.app.util.AppPreference
import com.forgivity.app.util.INTRO
import kotlinx.android.synthetic.main.activity_intro.*


class IntroActivity : BaseActivity() {

    // - - Initialize Objects
    private lateinit var introAdapter: IntroAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intro)
        ButterKnife.bind(this)
        initViewPager()
    }

    private fun initViewPager() {
        // Initializing the ViewPagerAdapter
        introAdapter = IntroAdapter(this)

        // Adding the Adapter to the ViewPager
        mViewPager.adapter = introAdapter

        // Setting Tab layout (Dot Indicator)
        dotIndicator.setupWithViewPager(mViewPager, true)
    }
}
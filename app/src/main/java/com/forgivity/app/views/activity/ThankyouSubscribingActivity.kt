package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.model.StatusMsgModel
import com.forgivity.app.model.WelcomePodModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.util.AppPreference
import com.forgivity.app.util.GOT_IT
import kotlinx.android.synthetic.main.activity_my_program.*
import kotlinx.android.synthetic.main.activity_my_program.imgMusicIV
import kotlinx.android.synthetic.main.activity_thankyou_subscribing.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ThankyouSubscribingActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_thankyou_subscribing)
        ButterKnife.bind(this)
        imgHeartIV.startAnimation(AnimationUtils.loadAnimation(mActivity,
            R.anim.zoom_in_out)
        )
    }

    @OnClick(
        R.id.txtNextTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtNextTV -> performNextClick()
        }
    }

    private fun performNextClick() {
        AppPreference().writeBoolean(mActivity, GOT_IT,false)
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeWelcomePodApi()
        }
    }

    private fun executeWelcomePodApi() {
        showProgressDialog(mActivity)
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.welcomePodRequest(getAccessToken()).enqueue(object :
            Callback<WelcomePodModel?> {
            override fun onResponse(
                call: Call<WelcomePodModel?>?,
                response: Response<WelcomePodModel?>
            ) {
                    dismissProgressDialog()
                val mModel: WelcomePodModel? = response.body()
                if (mModel?.status == 1) {
                    val i = Intent(mActivity, AudiooActivity::class.java)
                    i.putExtra("list", mModel.pod_detail)
                    i.putExtra("value", "Thankyou")
                    startActivity(i)
                } else {
                    dismissProgressDialog()
                    showToast(mActivity, mModel?.message)
                }
            }

            override fun onFailure(call: Call<WelcomePodModel?>?, t: Throwable?) {
                dismissProgressDialog()
                Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
            }
        })
    }


}

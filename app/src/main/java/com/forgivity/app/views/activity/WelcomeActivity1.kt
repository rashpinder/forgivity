package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.model.WelcomePodModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.util.AppPreference
import com.forgivity.app.util.ORG_ID
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WelcomeActivity1 : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome1)
        ButterKnife.bind(this)
    }

    @OnClick(
        R.id.btnGotItWelcome1
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.btnGotItWelcome1 -> performGotItClick()
        }
    }

    private fun performGotItClick() {
        if (AppPreference().readString(mActivity, ORG_ID,null).equals("1")){
            startActivity(Intent(mActivity, SubscriptionActivity1::class.java))
            finish()
        }
        else{
            performNextClick()
        }
//        startActivity(Intent(mActivity, WelcomeActivity2::class.java))
    }

    private fun performNextClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeWelcomePodApi()
        }
    }

    private fun executeWelcomePodApi() {
        showProgressDialog(mActivity)
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.welcomePodRequest(getAccessToken()).enqueue(object :
            Callback<WelcomePodModel?> {
            override fun onResponse(
                call: Call<WelcomePodModel?>?,
                response: Response<WelcomePodModel?>
            ) {
                dismissProgressDialog()
                val mModel: WelcomePodModel? = response.body()
                if (mModel?.status == 1) {
                    val i = Intent(mActivity, AudiooActivity::class.java)
                    i.putExtra("list", mModel.pod_detail)
                    i.putExtra("value", "Thankyou")
                    startActivity(i)
                    finish()
                } else {
                    dismissProgressDialog()
                    showToast(mActivity, mModel?.message)
                }
            }

            override fun onFailure(call: Call<WelcomePodModel?>?, t: Throwable?) {
                dismissProgressDialog()
                Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
            }
        })
    }



    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
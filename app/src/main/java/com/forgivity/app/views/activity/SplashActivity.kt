package com.forgivity.app.views.activity

import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.forgivity.app.R
import com.forgivity.app.model.LinkedDetails
import com.forgivity.app.util.*
import io.branch.referral.Branch
import io.branch.referral.Branch.BranchReferralInitListener
import org.json.JSONObject

class SplashActivity : AppCompatActivity() {
    var type = ""

    //    var org_id = ""
//    var mTitle = ""
//    var notify_id = ""
//    var linked_id = ""
    var mType = ""
    var org_id = ""
    var notify_id = 0
    var linked_id = 0
    var mLinkedList: LinkedDetails? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
//        setUpSplash()
    }


//    override fun onNewIntent(intent: Intent?) {
//        super.onNewIntent(intent)
//        setIntent(intent)
//        // if activity is in foreground (or in backstack but partially visible) launching the same
//        // activity will skip onStart, handle this case with reInitSession
//        Branch.sessionBuilder(this).withCallback(branchReferralInitListener).reInit()
//    }


//    private val branchReferralInitListener =
//        BranchReferralInitListener { linkProperties, error -> // do stuff with deep link data (nav to page, display content, etc)
////            if (isLogin()) {
////                if (linkProperties != null) {
////                    try {
//            if (error != null) {
//                if (linkProperties != null && linkProperties["+clicked_branch_link"] == true) {
//                    if (!linkProperties.isNull("type") && linkProperties["type"].toString() != "") {
//                        type = linkProperties["type"].toString()
//                    }
//                    if (!linkProperties.isNull("org_id") && linkProperties["org_id"].toString() != "") {
//                        org_id = linkProperties["org_id"].toString()
//                    }
//                    if (type != "" && type.equals("organisation")) {
//                        setUpSplash()
//                        AppPreference().writeString(
//                            this,
//                            ORG_ID,
//                            org_id
//                        )
//                    }
//                } else {
//                    setUpSplash()
//                    AppPreference().writeString(
//                        this,
//                        ORG_ID,
//                        "1"
//                    )
//                }
//            } else {
//                setUpSplash()
//                AppPreference().writeString(
//                    this,
//                    ORG_ID,
//                    "1"
//                )
//            }
////                    }
////                    catch (e: JSONException) {
////                        e.printStackTrace()
////                    }
//        }
//                }
//                 else {
//                    setUpSplash()
//                    AppPreference().writeString(
//                        this,
//                        ORG_ID,
//                        "1"
//                    )
//                }
//            }


    private fun setUpSplash() {
        val mThread = object : Thread() {
            override fun run() {
                sleep(SPLASH_TIME_OUT)
                if (AppPreference().readBoolean(this@SplashActivity, IS_LOGIN, false)) {
                    AppPreference().writeString(this@SplashActivity, CHECKIN, "true")
                    if (intent != null && intent.extras != null) {
                        for (key in intent.extras!!.keySet()) {
                            val value = intent.extras!!.getString(key)
                            Log.e(TAG, "Key:::: $key Value:::: $value")
                            var mJsonObject: JSONObject
                            if (key == "data") {
                                mJsonObject = JSONObject(value)
                                if (!mJsonObject.isNull("notification_type")) {
                                    mType = mJsonObject.getString("notification_type")
                                }
                                if (!mJsonObject.isNull("org_id")) {
                                    org_id = mJsonObject.getInt("org_id").toString()
                                }
                                if (!mJsonObject.isNull("notify_id")) {
                                    notify_id = mJsonObject.getInt("notify_id")
                                }
                                if (!mJsonObject.isNull("linked_id")) {
                                    linked_id = mJsonObject.getInt("linked_id")
                                }}}

//                            else{
//                                setUpSplash()
//                                AppPreference().writeString(
//                                    this@SplashActivity,
//                                    ORG_ID,
//                                    "1"
//                                )
//                            }
                            var mIntent: Intent? = null
                            if (mType.equals("mass")) {
                                mIntent = Intent(this@SplashActivity, MyProgramActivity::class.java)
                                startActivity(mIntent)
                                finish()
                            } else if (mType.equals("nugget")) {
                                mIntent = Intent(
                                    this@SplashActivity, MyProgramActivity
                                    ::class.java
                                )
                                mIntent.putExtra("mType", mType)
                                mIntent.putExtra("linked_id", linked_id)
                                mIntent.putExtra("receiver", true)
                                startActivity(mIntent)
                                finish()
                            } else if (mType.equals("podcast")) {
                                mIntent = Intent(this@SplashActivity, AudiooActivity::class.java)
                                mIntent.putExtra("mType", mType)
                                mIntent.putExtra("linked_id", linked_id)
                                mIntent.putExtra("receiver", true)
                                startActivity(mIntent)
                                finish()
                            } else if (mType.equals("survey")) {
                                mIntent = Intent(this@SplashActivity, MyProgramActivity::class.java)
                                startActivity(mIntent)
                                finish()
                            } else {
                                mIntent = Intent(this@SplashActivity, MyProgramActivity::class.java)
                                AppPreference().writeString(this@SplashActivity, CHECKIN, "true")
                                startActivity(mIntent)
                                finish()
                            }
//                            getLinkedDetails()
//                        }
//                            else{
//                                setUpSplash()
//                                AppPreference().writeString(
//                                    this@SplashActivity,
//                                    ORG_ID,
//                                    "1"
//                                )
////                            }


//                        }
                    } else {
                        if (AppPreference().readBoolean(
                                this@SplashActivity,
                                GOT_IT,
                                false
                            ) == true
                        ) {
                            val intent = Intent(this@SplashActivity, WelcomeActivity1::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                            startActivity(intent)
                            finish()
                        } else {
                            val i = Intent(this@SplashActivity, MyProgramActivity::class.java)
                            AppPreference().writeString(this@SplashActivity, CHECKIN, "true")
                            startActivity(i)
                            finish()
                        }
                    }
                }
                else if(AppPreference().readString(this@SplashActivity, INTRO, null).equals("true")){
                    AppPreference().writeString(this@SplashActivity, CHECKIN, "true")
                    val i = Intent(this@SplashActivity, StartActivity::class.java)
                    startActivity(i)
                    finish()
                    }
                    else{
                        AppPreference().writeString(this@SplashActivity, CHECKIN, "true")
                        val i = Intent(this@SplashActivity, IntroActivity::class.java)
                        startActivity(i)
                        finish()
                    }
            }
        }
        mThread.start()
    }  var mIntent: Intent? = null

    override fun onStart() {
        super.onStart()

//        Branch.sessionBuilder(this).withCallback(branchReferralInitListener)
//            .withData(if (intent != null) intent.data else null).init()

        val branch = Branch.getInstance()
        branch.setRetryCount(2)
        branch.initSession({ referringParams, error ->
            referringParams?.let {
                Log.e("rgggtgtgtgtgtgtg==", "hfch" + referringParams.toString())

                if (referringParams.get(
                        "+clicked_branch_link").equals(true) && !referringParams.get(
                        "+clicked_branch_link").equals(null)
                ) {
                initDeepLinkSignIn(referringParams)}
                else{
                    setUpSplash()
                    AppPreference().writeString(
                        this,
                        ORG_ID,
                        "1"
                    )
                }
            } ?: let {
                Log.e("Error", error.toString())
                setUpSplash()
                AppPreference().writeString(
                    this,
                    ORG_ID,
                    "1"
                )
            }
        }, this.intent.data, this)

    }

    private fun initDeepLinkSignIn(linkProperties: JSONObject) {
        if (linkProperties.has("type")){
            type = linkProperties["type"].toString()
            org_id = linkProperties["org_id"].toString()
        if (type != "" && type.equals("organisation")) {
            setUpSplash()
            AppPreference().writeString(
                this,
                ORG_ID,
                org_id
            )
        }
        else {
                setUpSplash()
                AppPreference().writeString(
                    this,
                    ORG_ID,
                    "1"
                )
            }
    } else {
            setUpSplash()
            AppPreference().writeString(
                this,
                ORG_ID,
                "1"
            )
        }


}}


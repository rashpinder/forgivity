package com.forgivity.app.views.activity

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.adapter.FeedAdapter
import com.forgivity.app.interfaces.LinkedDataInterface
import com.forgivity.app.interfaces.LoadMoreActivityFeedData
import com.forgivity.app.model.FeedActivityModel
import com.forgivity.app.model.LinkedDetails
import com.forgivity.app.model.LinkedDetailsModel
import com.forgivity.app.model.ListItemFeed
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.viewModel.FeedActivityViewModel
import kotlinx.android.synthetic.main.activity_feed.*
import kotlinx.android.synthetic.main.dialog_podcast.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FeedActivity : BaseActivity(), LoadMoreActivityFeedData, LinkedDataInterface {

    // - - Initialize Objects
    var mLoadMore: LoadMoreActivityFeedData? = null
    var mLinkedDataInterface: LinkedDataInterface? = null
    var mPageNo: Int = 1
    var isLoading: Boolean = true
    var type: String = ""
    var type_id: String = ""
    var mActivityFeedList: ArrayList<ListItemFeed?>? = ArrayList()

    //    var mLinkedList: ArrayList<LinkedDetails> = ArrayList()
    var mLinkedList: LinkedDetails? = null
    private var feedActivityViewModel: FeedActivityViewModel? = null
    private var feedAdapter: FeedAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feed)
        ButterKnife.bind(this)
        feedActivityViewModel = ViewModelProviders.of(this).get(FeedActivityViewModel::class.java)
        mLoadMore = this
        mLinkedDataInterface = this
        if (mActivityFeedList != null) {
            mActivityFeedList!!.clear()
        }
        getActivityFeedData()
    }

    @OnClick(
        R.id.imgBackIV
    )
    fun onClick(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
        }
    }

    private fun getActivityFeedData() {
        if (isNetworkAvailable(mActivity)) {
            executeFeedActivityListApi()
        } else {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        }
    }

    // - - Execute Activity List Api
    private fun executeFeedActivityListApi() {
        showProgressDialog(mActivity)
        feedActivityViewModel?.activityListData(mActivity, getAccessToken(), mPageNo.toString())
            ?.observe(this,
                androidx.lifecycle.Observer<FeedActivityModel?> { mModel ->
                    when (mModel.status) {
                        1 -> {
                            dismissProgressDialog()
                            isLoading = !mModel.allData?.lastPage?.equals(true)!!
                            mActivityFeedList = mModel!!.allData?.list
                            setAdapter()
                            noMoreDataTV.visibility = View.GONE
                        }
                        else -> {
                            showAlertDialog(mActivity, mModel.message)
                            dismissProgressDialog()
                            noMoreDataTV.visibility = View.VISIBLE
                        }
                    }
                })
    }

    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        activityFeedRV.layoutManager = layoutManager
        feedAdapter = FeedAdapter(mActivity, mActivityFeedList, mLoadMore,mLinkedDataInterface)
        activityFeedRV.adapter = feedAdapter
    }

    private fun loadMoreActivityFeedData() {
        progressLL.visibility = View.VISIBLE
        feedActivityViewModel?.activityListData(mActivity, getAccessToken(), mPageNo.toString())
            ?.observe(this, androidx.lifecycle.Observer<FeedActivityModel?> { mModel ->

                if (mModel.status == 1) {
                    progressLL.visibility = View.GONE
                    mModel.allData?.list?.let { mActivityFeedList?.addAll(it) }
                    feedAdapter?.notifyDataSetChanged()
                    isLoading = !mModel.allData?.lastPage?.equals(true)!!
                } else if (mModel.status == 0) {
                    progressLL.visibility = View.GONE
                }
            })
    }


    override fun onLoadMoreActivityFeedData(mModel: ListItemFeed) {
        if (isLoading) {
            ++mPageNo
            loadMoreActivityFeedData()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right)
    }


    private fun getLinkedDetails() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeLinkedApi()
        }
    }

    private fun executeLinkedApi() {
        showProgressDialog(mActivity)
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getLinkedDetailsRequest(getAccessToken(), type_id, type)
            .enqueue(object :
                Callback<LinkedDetailsModel?> {
                override fun onResponse(
                    call: Call<LinkedDetailsModel?>?,
                    response: Response<LinkedDetailsModel?>
                ) {
                    dismissProgressDialog()
                    val mModel: LinkedDetailsModel? = response.body()
                    if (mModel?.status == 1) {
                        mLinkedList = mModel.linked_details
                        if (mModel.linked_type.equals("mass")) {

                        } else if (mModel.linked_type.equals("nugget")) {
                            showPodcastAlertDialog(
                                mActivity,
                                mLinkedList!!.title,
                                mLinkedList!!.description,
                                mLinkedList!!.related_pod
                            )
                        } else if (mModel.linked_type.equals("podcast")) {
                            val i = Intent(mActivity, AudiooActivity::class.java)
                            i.putExtra("pod_file", mLinkedList!!.related_pod)
                            i.putExtra("pod_title", mLinkedList!!.pod_title)
                            i.putExtra("pod_type", mModel.linked_type)
                            startActivity(i)
//                            finish()
                        } else {

                        }

                    } else {
                        showToast(mActivity, mModel?.message)
                    }
                }

                override fun onFailure(call: Call<LinkedDetailsModel?>?, t: Throwable?) {
                    dismissProgressDialog()
                    Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
                }
            })
    }

    // - - Podcast Alert Dialog
    private fun showPodcastAlertDialog(mActivity: Any?, title: String, description: String,podFile: String) {
        val alertDialog = Dialog(mActivity!! as Context)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_podcast)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val imgCrossIV = alertDialog.findViewById<ImageView>(R.id.imgCrossIV)
        val txtTitleTV = alertDialog.findViewById<TextView>(R.id.txtTitleTV)
        val txtDescriptionTV = alertDialog.findViewById<TextView>(R.id.txtDescriptionTV)
        val txtRelatedListeningTV = alertDialog.findViewById<TextView>(R.id.txtRelatedListeningTV)
        txtTitleTV.text=title
        txtDescriptionTV.text=description
        if(podFile!=null && !podFile.equals("")){
            txtRelatedListeningTV.isVisible=true
        }
        else{
            txtRelatedListeningTV.isVisible=false
        }

        imgCrossIV.setOnClickListener {
            alertDialog.dismiss()
        }
        txtRelatedListeningTV.setOnClickListener {
            val i = Intent(this, AudiooActivity::class.java)
            i.putExtra("pod_file", podFile)
            i.putExtra("pod_id", mLinkedList!!.pod_id)
            i.putExtra("pod_title", mLinkedList!!.pod_title)
            i.putExtra("pod_desc", mLinkedList!!.pod_desc)
            i.putExtra("pod_type", mLinkedList!!.pod_type)
            startActivity(i)
        }
        alertDialog.show()
    }

    // - - Survey Result Alert Dialog
    private fun showSurveyResultAlertDialog(mActivity: Any?) {
        val alertDialog = Dialog(mActivity!! as Context)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_survey_three_result)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val btnDone = alertDialog.findViewById<TextView>(R.id.btnDone)
        btnDone.setOnClickListener {
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    override fun onItemClickListener(type_id: String, type: String) {
        this.type_id = type_id
        this.type = type
        getLinkedDetails()
    }
}
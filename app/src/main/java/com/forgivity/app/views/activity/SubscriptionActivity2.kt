package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.TransactionDetails
import com.forgivity.app.R
import com.forgivity.app.util.*

class SubscriptionActivity2 : BaseActivity(){

//    , BillingProcessor.IBillingHandler {

//    var mBillingProcessor: BillingProcessor? = null
//    var transactionDetails: TransactionDetails? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription2)
        ButterKnife.bind(this)
//        initializeSubscription()
    }
//    /*
//    * In App  Subscriptions
//    * */
//    open fun initializeSubscription() {
//        mBillingProcessor = BillingProcessor(mActivity, LICENCE_KEY, this)
//        mBillingProcessor!!.initialize()
//    }
//
//    private fun subscriptionCallBack() {
//        // With Testing  PayLoads for Product Purchase & Subscription
//        //mBillingProcessor.purchase(getActivity(), "android.test.purchased", null /*or developer payload*/, extraParams);
//        if (isNetworkAvailable(mActivity)) {
//            val extraParams = Bundle()
//            extraParams.putString("accountId", MERCHANT_ID)
//            mBillingProcessor!!.subscribe(mActivity, SUBSCRIPTION_PLAN_KEY, null , extraParams)
//        } else {
//            showToast(mActivity, getString(R.string.internet_connection_error))
//        }
//    }
//
//    override fun onProductPurchased(productId: String, mTransactionDetails: TransactionDetails?) {
//        Log.e(TAG, "Purchase DATA :- " + mTransactionDetails!!.purchaseInfo.purchaseData)
//        /*
//         * Called when requested PRODUCT ID was successfully purchased
//         */
//        val mPurchaseData = mTransactionDetails!!.purchaseInfo.purchaseData
//        var mSubscriptionPlanKey = mTransactionDetails!!.purchaseInfo.purchaseData.productId
//
//        Log.e(TAG, "onProduct Purchased_Data: " + mTransactionDetails!!.purchaseInfo.purchaseData.toString())
//        Log.e(TAG, "onProduct Purchased_OrderID: " + mPurchaseData!!.orderId)
//        Log.e(TAG, "onProduct Purchased_Token: " + mPurchaseData!!.purchaseToken)
//        Log.e(TAG, "onProduct Purchased_Time: " + mPurchaseData!!.purchaseTime)
//        Log.e(TAG, "Subscriptions Plan Key: $mSubscriptionPlanKey")
//
//
//        if (mTransactionDetails!!.purchaseInfo == null) {
//            AppPreference().writeBoolean(mActivity, IS_SUBSCRIPTION, false)
//            showToast(mActivity,getString(R.string.some_things))
//        } else {
//            AppPreference().writeBoolean(mActivity, IS_SUBSCRIPTION, true)
//            showToast(mActivity,getString(R.string.purchase_subscription_))
//        }
//        finish()
//    }
//
//    override fun onPurchaseHistoryRestored() {
//
//    }
//
//    override fun onBillingError(errorCode: Int, error: Throwable?) {
//        /*
//        * Called when some error occurred. See Constants class for more details
//        *
//        * Note - this includes handling the case where the user canceled the buy dialog:
//        * errorCode = Constants.BILLING_RESPONSE_RESULT_USER_CANCELED
//        */
//        Log.e(TAG, "onBillingError: ")
//        when (errorCode) {
//            0 -> Log.e("onBillingError", "> Success - BILLING_RESPONSE_RESULT_OK")
//            1 -> Log.e(
//                "onBillingError",
//                "> User pressed back or canceled a dialog - BILLING_RESPONSE_RESULT_USER_CANCELED"
//            )
//            3 -> Log.e(
//                "onBillingError",
//                "> Billing API version is not supported for the type requested - BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE"
//            )
//            4 -> Log.e(
//                "onBillingError",
//                "> Requested product is not available for purchase - BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE"
//            )
//            5 -> Log.e(
//                "onBillingError",
//                "> Invalid arguments provided to the API. This error can also indicate that the application was not correctly signed or properly set up for In-app Billing in Google Play, or does not have the necessary permissions in its manifest - BILLING_RESPONSE_RESULT_DEVELOPER_ERROR"
//            )
//            6 -> Log.e(
//                "onBillingError",
//                "> Fatal error during the API action - BILLING_RESPONSE_RESULT_ERROR"
//            )
//            7 -> Log.e(
//                "onBillingError",
//                "> Failure to purchase since item is already owned - BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED"
//            )
//            8 -> Log.e(
//                "onBillingError",
//                "> Failure to consume since item is not owned - BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED"
//            )
//        }
//    }
//
//    override fun onBillingInitialized() {
//        /*
//        * Called when BillingProcessor was initialized and it's ready to purchase
//        */
//        Log.e(TAG, "onBillingInitialized: ");
//    }
//
//    override fun onDestroy() {
//        if (mBillingProcessor != null) mBillingProcessor!!.release()
//        super.onDestroy()
//    }
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        if (!mBillingProcessor!!.handleActivityResult(requestCode, resultCode, data)) super.onActivityResult(
//            requestCode,
//            resultCode,
//            data
//        )
//    }


    @OnClick(
        R.id.txtBuyTV,
        R.id.txtTermsTV,
        R.id.txtPrivacyTV,
        R.id.txtRestoreTV

    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtBuyTV -> performClick()
            R.id.txtTermsTV -> performTermsConditionsClick()
            R.id.txtPrivacyTV -> performPrivacyPolicyClick()
            R.id.txtRestoreTV -> performRestoreClick()
        }
    }

    private fun performRestoreClick() {
//        onPurchaseHistoryRestored()
    }

    private fun performPrivacyPolicyClick() {
        val i = Intent(mActivity, WebViewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_PP)
        startActivity(i)
    }

    private fun performTermsConditionsClick() {
        val i = Intent(mActivity, WebViewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_TERMS)
        startActivity(i)
    }

    private fun performClick() {
//        if (mBillingProcessor!!.isSubscriptionUpdateSupported) {
//            subscriptionCallBack()
//        } else {
//            showToast(mActivity,getString(R.string.subscription_update_not))
//        }
        startActivity(Intent(mActivity, ThankyouSubscribingActivity::class.java))
        finish()
    }
}
package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.model.QuizDetailModel
import com.forgivity.app.model.RelatedQues
import com.forgivity.app.model.SubmitAnswerModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import kotlinx.android.synthetic.main.activity_result.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ResultActivity : BaseActivity() {

    var quizId: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        ButterKnife.bind(this)
        getIntentData()
        getQuizResultApi()
    }


    private fun getIntentData() {
        if (intent != null) {
            quizId = intent.getStringExtra("quiz_id").toString()
            Log.e(TAG, "list" + quizId)
        }
    }

    private fun getQuizResultApi() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeQuizResultRequest()
        }
    }

    private fun executeQuizResultRequest() {
        showProgressDialog(mActivity)
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getQuizResultRequest(getAccessToken(), quizId.toString())
            .enqueue(object :
                Callback<QuizDetailModel?> {
                override fun onResponse(
                    call: Call<QuizDetailModel?>?,
                    response: Response<QuizDetailModel?>
                ) {
                    dismissProgressDialog()
                    val mModel: QuizDetailModel? = response.body()
                    if (mModel?.status == 1) {
                        txtPercentageTV.text=mModel.quiz_detail+"%"
                    } else {
                        dismissProgressDialog()
                        showToast(mActivity, mModel?.message)
                    }
                }

                override fun onFailure(call: Call<QuizDetailModel?>?, t: Throwable?) {
                    dismissProgressDialog()
                    Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
                }
            })
    }



    @OnClick(
        R.id.btnGoBack,
        R.id.txtgotItTV,
        R.id.imgBackIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.btnGoBack -> performGoBackClick()
            R.id.imgBackIV -> performBackClick()
            R.id.txtgotItTV -> performGotItClick()
        }
    }

    private fun performGotItClick() {
        onBackPressed()
    }

    private fun performBackClick() {
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private fun performGoBackClick() {
        onBackPressed()
    }

//    private fun performBackClick() {
//        startActivity(Intent(mActivity, MyProgramActivity::class.java))
//        finish()
//
//    }
}
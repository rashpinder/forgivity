package com.forgivity.app.views.activity

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.adapter.RelatedQuesOptionsAdapter
import com.forgivity.app.adapter.ResultAdapter
import com.forgivity.app.interfaces.OnItemClickListener
import com.forgivity.app.model.*
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.util.AppPreference
import com.forgivity.app.util.SUBMIT_ANS_DONE
import com.forgivity.app.util.Utils
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_my_program.*
import kotlinx.android.synthetic.main.activity_review.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.component1
import kotlin.collections.component2

class ReviewActivity : BaseActivity() {
    var mOptionsList: ArrayList<Options> = ArrayList()
    var mResultList: ArrayList<Result> = ArrayList()
//    lateinit var mQuizQuesList: ArrayList<RelatedQues>
    var mRelatedQues: RelatedQues? = null
    var mNug_id = ""
    var quesId = ""
    var quesType = ""
    var optionKey = ""
    var nugID = ""
    private var screenshotLL:LinearLayout?=null
    lateinit var mRelatedOptionsAdapter: RelatedQuesOptionsAdapter
//    lateinit var mRelatedQuizAdapter: RelatedQuizAdapter
    lateinit var mResultAdapter: ResultAdapter
    lateinit var imgHeaderIV: ImageView
    /*
    * Permissions
    * */
    var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    val REQUEST_PERMISSION_CODE = 325
    var imagePathh: Uri? = null
    var mRelatedPod: RelatedPod? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review)
        ButterKnife.bind(this)
        getIntentData()
    }

    private fun getIntentData() {
//        imgHeaderIV.visibility = View.GONE
        if (intent != null) {
            mRelatedQues = intent.getParcelableExtra("list")
            if (mRelatedPod!=null){
            mRelatedPod = intent.getParcelableExtra("related_pod_list")}
//            if (mQuizQuesList!=null){
//            mQuizQuesList = intent.getParcelableArrayListExtra<RelatedQues>("quiz_questions_list")!!
//            }
            mNug_id = intent.getStringExtra("nug_id").toString()
            mOptionsList = intent.getParcelableArrayListExtra("options")!!
            Log.e(TAG, "list" + mRelatedQues)
            txtQuesTV.text = mRelatedQues?.quesDesc
            quesType = mRelatedQues?.quesType.toString()
            //options for related question
            setOptionsAdapter()
        }
    }

    private fun setOptionsAdapter() {
        mRelatedOptionsAdapter = RelatedQuesOptionsAdapter(
            mActivity,
            mRelatedQues,
            mOptionsList,
            mItemClickListner,
            mNug_id
        )
        optionsRV.layoutManager = LinearLayoutManager(mActivity)
        optionsRV.adapter = mRelatedOptionsAdapter
        mRelatedOptionsAdapter.notifyDataSetChanged()
//        imgHeaderIV.isVisible=false
    }

    @OnClick(
        R.id.imgBackIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> performBackClick()
        }
    }

    private fun performBackClick() {
        onBackPressed()
    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    //click of options
    var mItemClickListner: OnItemClickListener = object : OnItemClickListener {
        override fun onItemClickListner(quesId: String, optionKey: String, nugID: String) {
            this@ReviewActivity.quesId = quesId
            this@ReviewActivity.optionKey = optionKey
            this@ReviewActivity.nugID = nugID
            submitQuestionApi()
        }
    }

    /*
    submit selected answer api
     */
    private fun submitQuestionApi() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeSubmitAnswerRequest()
        }
    }

    private fun executeSubmitAnswerRequest() {
        showProgressDialog(mActivity)
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.submitAnswerRequest(getAccessToken(), quesId, optionKey, nugID)
            .enqueue(object :
                Callback<SubmitAnswerModel?> {
                override fun onResponse(
                    call: Call<SubmitAnswerModel?>?,
                    response: Response<SubmitAnswerModel?>
                ) {
                    dismissProgressDialog()
                    val mModel: SubmitAnswerModel? = response.body()
                    if (mModel?.status == 1) {

                        val hashMap = HashMap<String, String>() // Dummy HashMap.
                        val getResultCon = mModel.result // response Any
                        val gson = Gson()
                        val jsonElement = gson.toJsonTree(getResultCon)
                        var mLiiist = gson.fromJson(jsonElement, hashMap::class.java)

                        var index = 0
                        for ((key, value) in mLiiist) {
                            var keyByIndex = mLiiist.keys.elementAt(index) // Get key by index.
                            val valueOfElement = mLiiist.getValue(keyByIndex) // Get value.

                            var mModel: Result? = Result()
                            mModel?.result_key = keyByIndex
                            mModel?.result_value = valueOfElement
                            mResultList.add(mModel!!)
                            index++
                        }

//                       to Show Correct/Incorrect Popup
                        if (quesType.equals("2")) {
                            Log.e(TAG,"Option_key"+optionKey)
                            Log.e(TAG,"Option_key"+mRelatedQues?.correctOption)

                            if (optionKey.equals(mRelatedQues?.correctOption)) {
                                showCorrectAlertDialog(mActivity, mRelatedQues?.answerDesc,mRelatedPod)
                            } else {
                                showIncorrectAlertDialog(mActivity, mRelatedQues?.answerDesc,mRelatedPod)
                            }
                        }
//                       to Show Percentage Popup
                        else if (quesType.equals("0") || quesType.equals("1") || quesType.equals("3")) {
                            showSurveyResultAlertDialog(mActivity, mResultList,mRelatedQues?.quesDesc)
                        }
                    } else {
                        dismissProgressDialog()
                        showToast(mActivity, mModel?.message)
                    }
                }

                override fun onFailure(call: Call<SubmitAnswerModel?>?, t: Throwable?) {
                    dismissProgressDialog()
                    Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
                }
            })
    }


    /*
*
* Correct Alert Dialog
* */
    fun showSurveyResultAlertDialog(
        mActivity: Activity?,
        mResult: ArrayList<Result>,
        ansDesc:String?

    ) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_survey_three_result)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val btnDone = alertDialog.findViewById<TextView>(R.id.btnDone)
        val resultRV = alertDialog.findViewById<RecyclerView>(R.id.resultRV)
        val txtAnsTV = alertDialog.findViewById<TextView>(R.id.txtAnsTV)
        val imgShareIV = alertDialog.findViewById<ImageView>(R.id.imgShareIV)
        screenshotLL = alertDialog.findViewById(R.id.screenshotLL)
         imgHeaderIV = alertDialog.findViewById(R.id.imgHeaderIV)
        imgHeaderIV.visibility=View.GONE
        txtAnsTV.text=ansDesc
        mResultAdapter = ResultAdapter(
            mActivity,
            mResult
        )
        resultRV.layoutManager = LinearLayoutManager(mActivity)
        resultRV.adapter = mResultAdapter
//        imgHeaderIV.isVisible=false

//        mResultAdapter.notifyDataSetChanged()

        btnDone.setOnClickListener {
            alertDialog.dismiss()
            AppPreference().writeString(mActivity,SUBMIT_ANS_DONE,"done")
            finish()
        }

        imgShareIV.setOnClickListener {
            imgHeaderIV.visibility=View.VISIBLE
            imgHeaderIV.postDelayed(Runnable {
                performShareClick(
                    screenshotLL!!,
                    imgHeaderIV
                )
                                      }, 1000)

        }
        alertDialog.show()
    }

    /*
     *
     * Incorrect Answer Alert Dialog
     * */
    fun showIncorrectAlertDialog(mActivity: Activity?, strMessage: String?,mRelatedPod: RelatedPod?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_incorrect)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val txtRelatedListeningTV = alertDialog.findViewById<TextView>(R.id.txtRelatedListeningTV)
        val btnDoneIncorrect = alertDialog.findViewById<TextView>(R.id.btnDoneIncorrect)
        txtMessageTV.text = strMessage
        if (mRelatedPod!=null){
            txtRelatedListeningTV.isVisible=true
        }
        else{
            txtRelatedListeningTV.isVisible=false
        }
        btnDoneIncorrect.setOnClickListener {
            alertDialog.dismiss()
            AppPreference().writeString(mActivity,SUBMIT_ANS_DONE,"done")
            finish()
        }

        txtRelatedListeningTV.setOnClickListener {
//            alertDialog.dismiss()
            val i = Intent(mActivity, AudiooActivity::class.java)
            i.putExtra("list", mRelatedPod)
            startActivity(i)
        }

        alertDialog.show()
    }

    /*
     *
     * Correct Answer Alert Dialog
     * */
    fun showCorrectAlertDialog(mActivity: Activity?, strMessage: String?,mRelatedPod: RelatedPod?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_correct)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val txtTitleTV = alertDialog.findViewById<TextView>(R.id.txtTitleTV)
        val txtRelatedListeningTV = alertDialog.findViewById<TextView>(R.id.txtRelatedListeningTV)
        val btnDoneCorrect = alertDialog.findViewById<TextView>(R.id.btnDoneCorrect)
        txtMessageTV.text = strMessage
//        txtTitleTV.text = strMessage
        btnDoneCorrect.setOnClickListener {
            alertDialog.dismiss()
            AppPreference().writeString(mActivity,SUBMIT_ANS_DONE,"done")
            finish()
        }
        txtRelatedListeningTV.setOnClickListener {
//            alertDialog.dismiss()
            val i = Intent(mActivity, AudiooActivity::class.java)
            i.putExtra("list", mRelatedPod)
            startActivity(i)
        }
        alertDialog.show()
    }

    /*
   * Share Image Funcitonality:
   *
   * */

    private fun performShareClick(screenshotLL:LinearLayout,imgHeaderIV:ImageView) {
        if (checkPermission()) {
            takeScreenShotOfLayout(screenshotLL,imgHeaderIV)
        } else {
            requestPermission()
        }
    }


    fun saveImage(finalBitmap: Bitmap, resourceName: String, context: Context,imgHeaderIV: ImageView) {
        imgHeaderIV.visibility = View.VISIBLE
        val filename = resourceName

        //Output stream
        var fos: OutputStream? = null

        //For devices running android >= Q
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            //getting the contentResolver
            context?.contentResolver?.also { resolver ->

                //Content resolver will process the contentvalues
                val contentValues = ContentValues().apply {

                    //putting file information in content values
                    put(MediaStore.MediaColumns.DISPLAY_NAME,filename)
                    put(MediaStore.MediaColumns.MIME_TYPE, "image/png")
                    put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DCIM)
                }

                //Inserting the contentValues to contentResolver and getting the Uri
                val imageUri: Uri? =
                    resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
                imagePathh=imageUri
//                    resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
                //Opening an outputstream with the Uri that we got
                fos = imageUri?.let { resolver.openOutputStream(it) }

            }
            imgHeaderIV.visibility = View.GONE
        } else {
            //These for devices running on android < Q
            //So I don't think an explanation is needed here
            val imagesDir =
                Utils.outputPath
            val image = File(imagesDir, filename)
            imagePathh=image.toUri()
            fos = FileOutputStream(image)
            imgHeaderIV.visibility = View.GONE
        }

        fos?.use {
            //Finally writing the bitmap to the output stream that we opened
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 100, it)
//           context?.showT("Saved to Photos")
        }
    }

    private fun takeScreenShotOfLayout(screenshotLL:LinearLayout,imgHeaderIV: ImageView) {
//        imgHeaderIV.visibility = View.VISIBLE
        val bitmap = getScreenShotFromView(screenshotLL)
        // if bitmap is not null then
        // save it to gallery
        if (bitmap != null) {
            saveImage(bitmap, "screenshot.png", mActivity,imgHeaderIV)
            val share = Intent(Intent.ACTION_SEND)
            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
            share.type = "image/*"
            share.putExtra(Intent.EXTRA_STREAM,imagePathh)
            startActivity(Intent.createChooser(share, "Share Image"))
        }else{
            showToast(mActivity,getString(R.string.some_thing))
        }
    }


    private fun getScreenShotFromView(v: View): Bitmap? {
        // create a bitmap object
        var screenshot: Bitmap? = null
        try {
            // inflate screenshot object
            // with Bitmap.createBitmap it
            // requires three parameters
            // width and height of the view and
            // the background color
            screenshot = Bitmap.createBitmap(v.measuredWidth, v.measuredHeight, Bitmap.Config.ARGB_8888)
            // Now draw this bitmap on a canvas
            val canvas = Canvas(screenshot)
            v.draw(canvas)

        } catch (e: Exception) {
            Log.e("GFG", "Failed to capture screenshot because:" + e.message)
        }
        // return the bitmap
        return screenshot
    }


    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(this, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(this, writeReadStorage)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity,
            arrayOf(writeExternalStorage, writeReadStorage),
            REQUEST_PERMISSION_CODE
        )
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_PERMISSION_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission has been denied by user")
                } else {
                    //Perform Location Click Code
                    takeScreenShotOfLayout(screenshotLL!!,imgHeaderIV)
                }
            }
        }
    }


}
package com.forgivity.app.views.activity

import android.Manifest
import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.animation.doOnEnd
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import cdflynn.android.library.checkview.CheckView
import com.forgivity.app.R
import com.forgivity.app.model.*
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.util.*
import com.forgivity.app.viewModel.GetReadingViewModel
import com.forgivity.app.viewModel.SaveViewModel
import com.forgivity.app.viewModel.UnSaveViewModel
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.exoplayer2.source.MediaSourceFactory
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_audio.*
import kotlinx.android.synthetic.main.activity_my_program.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.component1
import kotlin.collections.component2
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import android.app.ActivityManager
import android.app.ActivityManager.RunningAppProcessInfo

import android.app.usage.UsageStats

import java.util.TreeMap

import java.util.SortedMap

import android.app.usage.UsageStatsManager
import android.view.LayoutInflater
import android.widget.Toast





class MyProgramActivity : BaseActivity(),Comparator<UsageStats> {

    // - - Initialize Objects
    private var saveViewModel: SaveViewModel? = null
    private var unSaveViewModel: UnSaveViewModel? = null
    private var mReadingViewModel: GetReadingViewModel? = null
    var mNuggetsList: ArrayList<RelatedNuggetsItem> = ArrayList()
    var mNuggetsReadOutList: ArrayList<String> = ArrayList()
    var mQuizReadOutList: ArrayList<String> = ArrayList()
    var mAllQuizQuesList: ArrayList<RelatedQues> = ArrayList()
    private var player: SimpleExoPlayer? = null
    private var isPlaying = true
    private var isSaved: Boolean = false
    private var isNugId: Boolean = false
    var day_id: String? = ""
    var day_no: String? = ""
    var mTitle: String? = ""
    var mReceiver: String? = ""
    var mType: String? = ""
    var linked_id: String? = ""
    var mDescription: String? = ""
    var day_desc: String? = ""
    var day_level: String? = ""
    var day_desc_read: Int? = 0
    var nug_id_begin: String? = "0"
    var nug_id: String? = ""
    var first: Boolean? = true
    var relatedVisibility: Boolean? = false
    var mRelatedQues: RelatedQues? = null
    var mOptions: Options? = null
    var mRelatedPod: RelatedPod? = null
    var mDayRelatedPod: RelatedPod? = null
    var mRelatedQuiz: RelatedQuiz? = null
    var position: Int = 0
    var posss: Int = 0
    lateinit var imagePath: File
    var mLinkedList: LinkedDetails? = null

    var usageStats:UsageStats?=null

    /*
    * Permissions
    * */
    var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    val REQUEST_PERMISSION_CODE = 325
    var imagePathh: Uri? = null
//    private var mUsageStatsManager: UsageStatsManager? = null
//    private var mInflater: LayoutInflater? = null
//    private var mPm: PackageManager? = null

    override fun onStop() {
        super.onStop()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_program)
        ButterKnife.bind(this)
        val builder = VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
//        printForegroundTask()

        /*
        viewModels
         */
        saveViewModel = ViewModelProviders.of(this).get(SaveViewModel::class.java)
        unSaveViewModel = ViewModelProviders.of(this).get(UnSaveViewModel::class.java)
        mReadingViewModel = ViewModelProviders.of(this).get(GetReadingViewModel::class.java)
        /*
        get INtent data
         */
        getIntentData()

        /*
        update checkin user
         */
        if (AppPreference().readString(mActivity, CHECKIN, "").equals("true")) {
            start_time= System.currentTimeMillis()
            AppPreference().writeLong(mActivity,STARTT_TIME_SPENT, start_time)
            executeUpdateCheckInApi()

        }
//        val cal = Calendar.getInstance()
//        cal.add(Calendar.DAY_OF_YEAR, -5)
//        usageStats = (mUsageStatsManager!!.queryUsageStats(
//            UsageStatsManager.INTERVAL_BEST,
//            cal.getTimeInMillis(), System.currentTimeMillis()
//        ) ?: return) as UsageStats?
//
////        mUsageStatsManager = getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager?
////        mInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater?
////        mPm = getPackageManager()
//        val pkgStats: Long = usageStats!!.getTotalTimeInForeground()
//        Log.e(TAG, "pkgStats: "+pkgStats)
    }


//    private fun printForegroundTask() {
//        var usageStats: UsageStats? = null
//
//        var PackageName = "Nothing"
//
//        var TimeInforground: Long = 500
//
//        var minutes = 500
//        var seconds:kotlin.Int = 500
//        var hours:kotlin.Int = 500
//        var mUsageStatsManager = getSystemService("usage's") as UsageStatsManager
//
//        var time = System.currentTimeMillis()
//
//        var stats: List<UsageStats> =
//            mUsageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000 * 10, time)
//
//
//        if(stats!=null)
//        {
//            var mySortedMap: SortedMap<Long, UsageStats> = TreeMap()
//
//            for (usageStats in stats) {
//                TimeInforground = usageStats.totalTimeInForeground
//                PackageName = usageStats.packageName
//                minutes = (TimeInforground / (1000 * 60) % 60).toInt()
//                seconds = (TimeInforground / 1000).toInt() % 60
//                hours = (TimeInforground / (1000 * 60 * 60) % 24).toInt()
//                Log.i(
//                    "BAC",
//                    "PackageName is" + PackageName + "Time is: " + hours + "h" + ":" + minutes + "m" + seconds + "s"
//                )
//            }
//        }
//
//
//    }


    private fun executeUpdateCheckInApi() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeUpdateCheckInApiRequest()

        }
    }



    private fun executeUpdateCheckInApiRequest() {
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.updateCheckInRequest(
            getAccessToken(),
            AppPreference().readLong(mActivity, TOTAL_TIME_SPENT, 0).toString()
        )
            .enqueue(object :
                Callback<StatusMsgModel?> {
                override fun onResponse(
                    call: Call<StatusMsgModel?>?,
                    response: Response<StatusMsgModel?>
                ) {
                    val mModel: StatusMsgModel? = response.body()
                    if (mModel?.status == 1) {
//                        showToast(mActivity, mModel.message)
                        AppPreference().writeString(mActivity, CHECKIN, "false")
                        Log.e(TAG, "onResponse1111: "+AppPreference().readString(mActivity, CHECKIN, "") )
                    } else {
//                        showToast(mActivity, mModel?.message)
                    }
                }

                override fun onFailure(call: Call<StatusMsgModel?>?, t: Throwable?) {
                    Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
                }
            })
    }





    private fun getIntentData() {
        if (intent != null) {
            mTitle = intent.getStringExtra("nug_title")
            mReceiver = intent.getStringExtra("receiver")
            mDescription = intent.getStringExtra("description")
            linked_id = intent.getIntExtra("linked_id", 0).toString()
            mType = intent.getStringExtra("mType")

            if (!mTitle.equals("") && !mTitle.equals(null)) {
                showPodcastAlertDialog(
                    mActivity,
                    mTitle.toString(),
                    mDescription.toString()
                )
            }
            if (!mType.equals("") && !mType.equals(null)) {
                getLinkedDetails()
            }
        }
    }

    private fun getLinkedDetails() {
        if (!isNetworkAvailable(this)) {
            showAlertDialog(this, getString(R.string.internet_connection_error))
        } else {
            executeLinkedApi()
        }
    }

    private fun executeLinkedApi() {
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getLinkedDetailsRequest(
            getAccessToken(),
            linked_id.toString(), mType.toString()
        )
            .enqueue(object :
                Callback<LinkedDetailsModel?> {
                override fun onResponse(
                    call: Call<LinkedDetailsModel?>?,
                    response: Response<LinkedDetailsModel?>
                ) {
                    val mModel: LinkedDetailsModel? = response.body()
                    isNugId = false
                    if (mModel?.status == 1) {
                        mLinkedList = mModel.linked_details
                        showPodcastAlertDialog(
                            mActivity,
                            mLinkedList!!.title,
                            mLinkedList!!.description
                        )
                    } else {
                        showToast(mActivity, mModel?.message)
                    }
                }

                override fun onFailure(call: Call<LinkedDetailsModel?>?, t: Throwable?) {
                    Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
                }
            })
    }


    // - - Podcast Alert Dialog
    private fun showPodcastAlertDialog(mActivity: Any?, title: String, description: String) {
        val alertDialog = Dialog(mActivity!! as Context)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_podcast)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val imgCrossIV = alertDialog.findViewById<ImageView>(R.id.imgCrossIV)
        val txtTitleTV = alertDialog.findViewById<TextView>(R.id.txtTitleTV)
        val txtDescriptionTV = alertDialog.findViewById<TextView>(R.id.txtDescriptionTV)
        txtTitleTV.text = title
        txtDescriptionTV.text = description

        imgCrossIV.setOnClickListener {
            alertDialog.dismiss()
        }
        alertDialog.show()
    }


    override fun onResume() {
        super.onResume()
        /*
      get all readings api
       */
        if(AppPreference().readString(mActivity, LEAVE_HINT, "").equals("true")){
            AppPreference().writeString(this, LEAVE_HINT, "false")
            start_time= System.currentTimeMillis()
            AppPreference().writeLong(mActivity,STARTT_TIME_SPENT, start_time+ AppPreference().readLong(mActivity,NEW_STARTT_TIME_SPENT, 0))
        }

        if (AppPreference().readString(mActivity, SUBMIT_ANS_DONE, null).equals("done")) {
            getReadStatusRequest()
            isNugId = false
            AppPreference().writeString(mActivity, SUBMIT_ANS_DONE, "not_done")
        }
        else if (AppPreference().readString(mActivity,"Congrats",null).equals("true")){
            AppPreference().writeString(mActivity, "Congrats","false")
        }
        else {
            isNugId = false
            getReadingDetails()
        }

    }

    @OnClick(
        R.id.btnGotItHome1,
        R.id.btnBackHome,
        R.id.drawerRL,
        R.id.imgSaveIV,
        R.id.txtRelatedListeningTV,
        R.id.imgShareIV,
        R.id.txtBeginTV,
        R.id.txtBackTV,
        R.id.txtLetterBackTV,
        R.id.txtLetterBeginTV,
        R.id.imgDayMusicIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.btnGotItHome1 -> performGotItClick()
            R.id.drawerRL -> performDrawerClick()
            R.id.imgSaveIV -> performSaveClick()
            R.id.txtRelatedListeningTV -> performRelatedListeningClick()
            R.id.btnBackHome -> performBackClick()
            R.id.imgShareIV -> performShareClick()
            R.id.txtBeginTV -> performBeginClick()
            R.id.txtBackTV -> performBackdayClick()
            R.id.txtLetterBackTV -> performLetterBackClick()
            R.id.txtLetterBeginTV -> performLetterBeginClick()
            R.id.imgDayMusicIV -> performDayMusicClick()
        }
    }

    private fun performDayMusicClick() {
        val i = Intent(mActivity, AudiooActivity::class.java)
        i.putExtra("list", mDayRelatedPod)
        AppPreference().writeString(mActivity, "Congrats","true")
        startActivity(i)
    }

    private fun performLetterBeginClick() {
        pauseAudio()
        flipCard(mActivity, newdayLL, newLetterLL)
        txtTitleTV.text = day_desc
        Log.e(TAG, "msg" + day_desc)
        txtBackTV.isVisible = !day_id.equals("1")
    }

    private fun performLetterBackClick() {
//        pauseAudio()
        releasePlayer()
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeUnsetLetterApi()
        }
    }

    private fun executeUnsetLetterApi() {
        showProgressDialog(mActivity)
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.unsetReadStatusRequest(getAccessToken(), day_id!!).enqueue(object :
            Callback<StatusMsgModel?> {
            override fun onResponse(
                call: Call<StatusMsgModel?>?,
                response: Response<StatusMsgModel?>
            ) {
                dismissProgressDialog()
                val mModel: StatusMsgModel? = response.body()
                if (mModel?.status == 1) {
                    flipCardLeft(mActivity, myProgramLL, newLetterLL)
                    executeApi()
                    isNugId = false
//                    getReadingDetails()
                } else {
                    showToast(mActivity, mModel?.message)
                }
            }

            override fun onFailure(call: Call<StatusMsgModel?>?, t: Throwable?) {
                dismissProgressDialog()
                Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
            }
        })
    }

    private fun performBeginClick() {
        getReadStatussRequest()
    }


    private fun getReadStatussRequest() {
        if (!isNetworkAvailable(mActivity)) {
            imgHeaderIV.visibility = View.GONE
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeGetReadinggStatusRequest()
        }
    }

    // - - Execute getReading Status  Api
    private fun executeGetReadinggStatusRequest() {
        showProgressDialog(mActivity)
        day_id?.let {
            nug_id_begin?.let { it1 ->
                mReadingViewModel?.getReadStatusRequest(mActivity, getAccessToken(), it, it1)
                    ?.observe(this,
                        androidx.lifecycle.Observer<StatusMsgModel?> { mModel ->
                            when (mModel.status) {
                                1 -> {
                                    flipCard(mActivity, myProgramLL, newdayLL)
                                    dismissProgressDialog()
                                }
                                else -> {
                                    showAlertDialog(mActivity, mModel.message)
                                    dismissProgressDialog()
                                }
                            }
                        })
            }
        }
    }

    /*
    back of day drop
     */
    private fun performBackdayClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeUnsettApi()
        }
    }

    private fun executeUnsettApi() {
        showProgressDialog(mActivity)
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.unsetReadStatusRequest(getAccessToken(), day_id!!).enqueue(object :
            Callback<StatusMsgModel?> {
            override fun onResponse(
                call: Call<StatusMsgModel?>?,
                response: Response<StatusMsgModel?>
            ) {

                dismissProgressDialog()
                val mModel: StatusMsgModel? = response.body()
                if (mModel?.status == 1) {
                    flipCardLeft(mActivity, myProgramLL, newdayLL)
                    executeApi()
                    isNugId = false
                } else {
                    showToast(mActivity, mModel?.message)
                }
            }

            override fun onFailure(call: Call<StatusMsgModel?>?, t: Throwable?) {
                dismissProgressDialog()
                Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
            }
        })
    }


    private fun getReadingDetails() {
        if (!isNetworkAvailable(mActivity)) {
            imgHeaderIV.visibility = View.GONE
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeApi()
        }
    }

    private fun executeApi() {
        mNuggetsList.clear()
        mNuggetsReadOutList.clear()
        if (first == true) {
            showProgressDialog(mActivity)
            first = false
        }
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getReadingRequest(getAccessToken()).enqueue(object :
            Callback<ReadingModel?> {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onResponse(
                call: Call<ReadingModel?>?,
                response: Response<ReadingModel?>
            ) {
                dismissProgressDialog()
                imgHeaderIV.visibility = View.GONE
                val mModel: ReadingModel? = response.body()
                if (mModel?.status == 1) {
                    mNuggetsList = mModel.dayDetail!!.relatedNuggets
                    txtDayTV.text = mModel.dayDetail.dayDesc
                    day_id = mModel.dayDetail.dayId
                    day_no = mModel.dayDetail.dayNo
                    day_desc = mModel.dayDetail.dayDesc
                    day_level = mModel.dayDetail.dayLevel
                    day_desc_read = mModel.dayDetail.dayDescRead
                    /*
                     showing levels
                     */
                    if (day_level.equals("F")) {
                        imgForgiveIV.setImageDrawable(getDrawable(R.drawable.level_one_f))
                    } else if (day_level.equals("O")) {
                        imgForgiveIV.setImageDrawable(getDrawable(R.drawable.level_two_o))
                    } else if (day_level.equals("R")) {
                        imgForgiveIV.setImageDrawable(getDrawable(R.drawable.level_three_r))
                    } else if (day_level.equals("G")) {
                        imgForgiveIV.setImageDrawable(getDrawable(R.drawable.level_four_g))
                    } else if (day_level.equals("I")) {
                        imgForgiveIV.setImageDrawable(getDrawable(R.drawable.level_five_i))
                    } else if (day_level.equals("V")) {
                        imgForgiveIV.setImageDrawable(getDrawable(R.drawable.level_six_v))
                    } else if (day_level.equals("E")) {
                        imgForgiveIV.setImageDrawable(getDrawable(R.drawable.level_seven_e))
                    }


                    for (i in mNuggetsList.indices) {
                        /*
                         to get readout list
                         */
                        if (mNuggetsList.get(i).readStatus == 1) {
                            mNuggetsReadOutList.add(mNuggetsList.get(i).toString())
                        }

                        /*
                        to get first unread nugget
                         */
                        if (isNugId.equals(false) && mNuggetsList.get(i).readStatus == 0) {
                            txtNuggetDescriptionTV.text = mNuggetsList.get(i).nugDesc
                            nug_id = mNuggetsList.get(i).nugId
                            position = i

                            /*
                       to show saved/unsaved nugget
                       */
                            if (mNuggetsList.get(position).nugSaveStatus!!.equals(false)) {
                                imgSaveIV.setImageResource(R.drawable.ic_save)
                            } else {
                                imgSaveIV.setImageResource(R.drawable.ic_bookmark)
                            }
                            Log.e(TAG, "position" + position)
                            isNugId = true

                            /*
                        for hide/show realted listening text
                       */
                        if(mModel.dayDetail.relatedNuggets[i].relatedAudio.equals("0")){
                            txtRelatedListeningTV.visibility =View.GONE
                            relatedVisibility=false
                        }
                        else{
                            txtRelatedListeningTV.visibility =View.VISIBLE
                            relatedVisibility=true
                        }


                        }

                        /*
                      to get related questions
                       */
                        if (mModel.dayDetail.relatedNuggets[i].relatedQues is Any) {
                            if (mModel.dayDetail.relatedNuggets[i].relatedQues == "0") {
                                Log.e(TAG, "relatedQues 0")
                            } else {
                                val getResultCon =
                                    mModel.dayDetail.relatedNuggets[i].relatedQues // response Any
                                val gson = Gson()
                                val jsonElement = gson.toJsonTree(getResultCon)
                                mRelatedQues = gson.fromJson(jsonElement, RelatedQues::class.java)
                                Log.e(TAG, "relatedQues" + mRelatedQues)
                            }
                        }
                        /*
                        for hide/show realted listening text
                       */
//                        if(mModel.dayDetail.relatedNuggets[i].relatedAudio.equals("0")){
//                            txtRelatedListeningTV.visibility =View.GONE
//                            relatedVisibility=false
//                        }
//                        else{
//                            txtRelatedListeningTV.visibility =View.VISIBLE
//                            relatedVisibility=true
//                        }
//                        txtRelatedListeningTV.isVisible =
//                            !mModel.dayDetail.relatedNuggets[i].relatedAudio.equals("0")
//                        relatedVisibility =
//                            !mModel.dayDetail.relatedNuggets[i].relatedAudio.equals("0")


                        /*
                        getting related quiz
                         */
                        if (mModel.dayDetail.relatedNuggets[i].relatedQuiz is Any) {
                            if (mModel.dayDetail.relatedNuggets[i].relatedQuiz == "0") {

                            } else {
                                val getResultCon =
                                    mModel.dayDetail.relatedNuggets[i].relatedQuiz // response Any
                                val gson = Gson()
                                val jsonElement = gson.toJsonTree(getResultCon)
                                mRelatedQuiz = gson.fromJson(jsonElement, RelatedQuiz::class.java)
                            }
                        }

                        /*
                        getting related pods
                         */
                        if (mModel.dayDetail.relatedNuggets[i].relatedPod is Any) {
                            if (mModel.dayDetail.relatedNuggets[i].relatedPod == "0") {
//                                txtRelatedListeningTV.visibility =View.GONE
//                                relatedVisibility=false
                            } else {
                                val getResultCon = mModel.dayDetail.relatedNuggets[i].relatedPod
                                val gson = Gson()
                                val jsonElement = gson.toJsonTree(getResultCon)
                                mRelatedPod = gson.fromJson(jsonElement, RelatedPod::class.java)
//                                txtRelatedListeningTV.visibility =View.VISIBLE
//                                relatedVisibility=true
                            }
                        } else {
                            Log.e("GetReadingViewModel", "******ERROR******")
                        }
                    }

                    /*
                 setting progress bar according to readout nugget list size
                   */
                    progressBar.max = mNuggetsList.size
                    Log.e(mActivity.toString(), "prooo" + progressBar.progress)
                    progressBar.progress = mNuggetsReadOutList.size+1

                    if (mModel.dayDetail.letter_drop != null) {
                        if (day_desc_read == 0 && !mModel.dayDetail.letter_drop.pod_file.equals("")
                        ) {
                            flipCard(mActivity, newLetterLL, myProgramLL)
                            txtLetterTV.text = "LETTER " + mModel.dayDetail.dayLevel
                            txtLetterTitleTV.text = mModel.dayDetail.letter_drop.pod_desc
                            txtLetterBackTV.isVisible = !mModel.dayDetail.dayLevel.equals("F")
                            initializePlayer(mModel.dayDetail.letter_drop.pod_file.toString())
                            imgMusicIV.startAnimation(
                                AnimationUtils.loadAnimation(
                                    mActivity,
                                    R.anim.rotator
                                )
                            )

                        }
                    } else if ((day_desc_read == 0) && (mNuggetsReadOutList.size == 0)) {
                        flipCard(mActivity, newdayLL, myProgramLL)
                        txtTitleTV.text = day_desc
                        Log.e(TAG, "msg" + day_desc)
                        txtBackTV.isVisible = !day_id.equals("1")

                        if (mModel.dayDetail.relatedPod is Any) {
                            if (mModel.dayDetail.relatedPod == "0") {
                                imgDayMusicIV.isVisible = false
                            } else {
                                imgDayMusicIV.isVisible = true
                                val getResultCon = mModel.dayDetail.relatedPod
                                val gson = Gson()
                                val jsonElement = gson.toJsonTree(getResultCon)
                                mDayRelatedPod = gson.fromJson(jsonElement, RelatedPod::class.java)
                            }


                        }
                    }} else if (mModel?.status == 0) {
                        showToast(mActivity, mModel?.message)
                        val i = Intent(mActivity, CongratsActivity::class.java)
//                                        i.putExtra("message",mModel.message)
                        startActivity(i)
                    } else {
                        showToast(mActivity, mModel?.message)
                    }
                }

                override fun onFailure(call: Call<ReadingModel?>?, t: Throwable?) {
                    dismissProgressDialog()
                    Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
                }
            })
        }



            @SuppressLint("ResourceType")
            fun flipCard(context: Context, visibleView: View, inVisibleView: View) {
                try {
                    visibleView.isVisible = true
//            inVisibleView.isVisible=false
                    val flipOutAnimatorSet =
                        AnimatorInflater.loadAnimator(
                            context,
                            R.anim.rotate_out
                        ) as AnimatorSet
                    flipOutAnimatorSet.setTarget(inVisibleView)
                    val flipInAnimationSet =
                        AnimatorInflater.loadAnimator(
                            context,
                            R.anim.rotate_in
                        ) as AnimatorSet
                    flipInAnimationSet.setTarget(visibleView)
                    flipOutAnimatorSet.start()
                    flipInAnimationSet.start()
                    flipInAnimationSet.doOnEnd {
                        inVisibleView.isVisible = false
                    }
                } catch (e: Exception) {

                }
            }

            @SuppressLint("ResourceType")
            fun flipCardLeft(context: Context, visibleView: View, inVisibleView: View) {
                try {
                    visibleView.isVisible = true
                    val flipOutAnimatorSet =
                        AnimatorInflater.loadAnimator(
                            context,
                            R.anim.rotate_out
                        ) as AnimatorSet
                    flipOutAnimatorSet.setTarget(inVisibleView)
                    val flipInAnimationSet =
                        AnimatorInflater.loadAnimator(
                            context,
                            R.anim.rotate_in
                        ) as AnimatorSet
                    flipInAnimationSet.setTarget(visibleView)
                    flipOutAnimatorSet.start()
                    flipInAnimationSet.start()
                    flipInAnimationSet.doOnEnd {
                        inVisibleView.isVisible = false
                    }
                } catch (e: Exception) {

                }
            }

            fun initializePlayer(audio: String) {
                try {
                    releasePlayer()
                    val mediaDataSourceFactory: DataSource.Factory =
                        DefaultDataSourceFactory(
                            mActivity,
                            Util.getUserAgent(mActivity, "LettersSample"),
                            null
                        )

                    val mediaSource = ProgressiveMediaSource.Factory(mediaDataSourceFactory)
                        .createMediaSource(MediaItem.fromUri(audio))

                    val mediaSourceFactory: MediaSourceFactory =
                        DefaultMediaSourceFactory(mediaDataSourceFactory)

                    player = SimpleExoPlayer.Builder(mActivity)
                        .setMediaSourceFactory(mediaSourceFactory)
                        .build()

                    player!!.addMediaSource(mediaSource)
                    player!!.prepare()
                    player!!.repeatMode = SimpleExoPlayer.REPEAT_MODE_ONE
                    player!!.playWhenReady = true
//            audioTimer()
                    val ama = getSystemService(AUDIO_SERVICE) as AudioManager
                    // Request audio focus for playback
                    val result = ama.requestAudioFocus(
                        focusChangeListener,  // Use the music stream.
                        AudioManager.STREAM_MUSIC,  // Request permanent focus.
                        AudioManager.AUDIOFOCUS_GAIN
                    )
//            initAudioControls()
                    if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                        // other app had stopped playing song now , so u can do your stuff now .
                    }
                } catch (e: Exception) {
                    Log.e("ExoPlayerFeaturedTab", e.message.toString())
                }
            }


                    private
                    val focusChangeListener =
                        AudioManager.OnAudioFocusChangeListener { focusChange ->
                            try {
                                val mediaPlayerBackground = MediaPlayer()

                                when (focusChange) {
                                    AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK ->                                 // Lower the volume while ducking.
                                    {
                                        mediaPlayerBackground.setVolume(0.2f, 0.2f)
                                        pauseAudio()
                                    }
                                    AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> {
                                        mediaPlayerBackground.stop()
                                        pauseAudio()
                                    }
                                    AudioManager.AUDIOFOCUS_LOSS -> {
                                        mediaPlayerBackground.stop()
                                        pauseAudio()
                                    }
                                    AudioManager.AUDIOFOCUS_GAIN -> {
                                        // Return the volume to normal and resume if paused.
                                        mediaPlayerBackground.setVolume(1f, 1f)
                                        mediaPlayerBackground.start()
                                    }
                                    else -> {
                                    }
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }


                                private fun pauseAudio() {
                            if (player != null && player!!.isPlaying) {
//            imgPlayPauseAudioIV.setImageResource(R.drawable.ic_play_rl)
                                player!!.playWhenReady = false
                                player!!.playbackState
                                isPlaying = false
                            }
                        }


            fun releasePlayer() {
                if (player != null) {
                    player!!.playWhenReady = false
                    player!!.stop()
                    player!!.release()
                    player = null
                }
            }

                    private fun getReadStatusRequest() {
                if (!isNetworkAvailable(mActivity)) {
                    imgHeaderIV.visibility = View.GONE
                    showAlertDialog(mActivity, getString(R.string.internet_connection_error))
                } else {
                    executeGetReadingStatusRequest()
                }
            }


                    // - - Execute UnSave Data Api
                    private fun executeGetReadingStatusRequest() {
                showProgressDialog(mActivity)
                day_id?.let {
                    nug_id?.let { it1 ->
                        mReadingViewModel?.getReadStatusRequest(
                            mActivity,
                            getAccessToken(),
                            it,
                            it1
                        )
                            ?.observe(this,
                                androidx.lifecycle.Observer<StatusMsgModel?> { mModel ->
                                    when (mModel.status) {
                                        1 -> {
                                            imgHeaderIV.visibility = View.GONE
                                            dismissProgressDialog()
                                            Log.e(TAG, "list**********" + mNuggetsReadOutList.size)
                                            Log.e(TAG, "list**********" + ((mNuggetsList.size) - 1))
                                            executeApi()
                                            }
                                        else -> {
                                            showAlertDialog(mActivity, mModel.message)
                                            dismissProgressDialog()
                                        }
                                    }
                                })
                    }
                }
            }


                    private fun performSaveClick() {
                if (isNetworkAvailable(mActivity)) {
                    if (mNuggetsList.get(position).nugSaveStatus!!.equals(false)) {
                        executeSaveDataRequest()
                    } else {
                        executeUnSaveDataRequest()
                    }

                } else {
                    showAlertDialog(mActivity, getString(R.string.internet_connection_error))
                }
            }


                    private fun performBackClick() {
                if (!isNetworkAvailable(mActivity)) {
                    showAlertDialog(mActivity, getString(R.string.internet_connection_error))
                } else {
                    executeUnsetApi()
                }
            }

                    private fun executeUnsetApi() {
                mNuggetsList.clear()
                showProgressDialog(mActivity)
                val apiInterface: ApiInterface =
                    ApiClient.apiClient!!.create(ApiInterface::class.java)
                apiInterface.unsetReadStatusRequest(getAccessToken(), day_id!!).enqueue(object :
                    Callback<StatusMsgModel?> {
                    override fun onResponse(
                        call: Call<StatusMsgModel?>?,
                        response: Response<StatusMsgModel?>
                    ) {
//                    dismissProgressDialog()
                        val mModel: StatusMsgModel? = response.body()
                        if (mModel?.status == 1) {
                            executeApi()
                            isNugId = false
                        } else {
                            dismissProgressDialog()
                            showToast(mActivity, mModel?.message)
                        }
                    }

                    override fun onFailure(call: Call<StatusMsgModel?>?, t: Throwable?) {
                        dismissProgressDialog()
                        Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
                    }
                })
            }

                    /*
                       related listening click llistener
                       */
                    private fun performRelatedListeningClick() {
                val i = Intent(mActivity, AudiooActivity::class.java)
                i.putExtra("list", mRelatedPod)
                startActivity(i)

            }

                    private fun performGotItClick() {
                preventMultipleClick()
//                if (mNuggetsList != null) {
                    if(mNuggetsReadOutList.size.equals(mNuggetsList.size-1)){
                        val i = Intent(mActivity, CongratsActivity::class.java)
                        i.putExtra("day_no",day_no)
                        startActivity(i)
                    }
                    if (mNuggetsList.size > 0) {
                        if (mNuggetsList.get(position).relatedQues != "0") {
                            if (mRelatedQues?.answerStatus == true) {
                                isNugId = false
                                getReadStatusRequest()
                            } else {
                                val i = Intent(mActivity, ReviewActivity::class.java)
                                i.putExtra("list", mRelatedQues)
                                if (relatedVisibility == true) {
                                    i.putExtra("related_pod_list", mRelatedPod)
                                } else {
                                }
                                val hashMap = HashMap<String, String>() // Dummy HashMap.
                                var mOptionsList: ArrayList<Options> = ArrayList()
                                val getResultCon = mRelatedQues?.options // response Any
                                val gson = Gson()
                                val jsonElement = gson.toJsonTree(getResultCon)
                                var mLiiist = gson.fromJson(jsonElement, hashMap::class.java)

                                var index = 0
                                for ((key, value) in mLiiist) {
                                    var keyByIndex =
                                        mLiiist.keys.elementAt(index) // Get key by index.
                                    val valueOfElement = mLiiist.getValue(keyByIndex) // Get value.
                                    var mModel: Options? = Options()
                                    mModel?.option_key = keyByIndex
                                    mModel?.option_value = valueOfElement
                                    mOptionsList.add(mModel!!)
                                    index++
                                }
                                i.putExtra("options", mOptionsList)
                                i.putExtra("nug_id", mNuggetsList.get(position).nugId)
                                startActivity(i)
                                overridePendingTransitionSlideUPEnter()
                            }
                        } else {
                            if (mNuggetsList.get(position).relatedQuiz != "0") {
                                if (mNuggetsList.get(position).relatedQuiz is Any) {
                                    val getResultCon =
                                        mNuggetsList.get(position).relatedQuiz
                                    val gson = Gson()
                                    val jsonElement = gson.toJsonTree(getResultCon)
                                    mRelatedQuiz =
                                        gson.fromJson(jsonElement, RelatedQuiz::class.java)
                                    if (mRelatedQuiz?.questions != null) {
                                        var mQuizPendingList: ArrayList<RelatedQues> = ArrayList()
//                                    mRelatedQuiz?.questions as ArrayList<RelatedQues>

                                        mAllQuizQuesList.clear()
                                        mQuizReadOutList.clear()

                                        var mAllQuizQuesList =
                                            mRelatedQuiz?.questions as ArrayList<RelatedQues>

                                        for (i in mAllQuizQuesList.indices) {
                                            if (mAllQuizQuesList[i].answerStatus!!.equals(true)) {
                                                mQuizReadOutList.add(
                                                    mAllQuizQuesList.get(i).toString()
                                                )
                                                if (mAllQuizQuesList.size.equals(mQuizReadOutList.size)) {
//                                            showToast(mActivity, "All questions Answered")
                                                    getReadStatusRequest()
                                                    isNugId = false
                                                }
                                            } else if (mAllQuizQuesList[i].answerStatus!!.equals(
                                                    false
                                                )
                                            ) {
                                                mQuizPendingList.add(mAllQuizQuesList.get(i))
//                                        showToast(
//                                            mActivity,
//                                            "there is some related Quiz, coming soon!!!!!!"
//                                        )
                                            } else {

                                            }
                                        }

                                        Log.e(TAG, "********list*********" + mQuizPendingList)
                                        Log.e(TAG, "********list*********" + mAllQuizQuesList.size)


//                                for (i in mAllQuizQuesList.indices) {
//                                     if(mAllQuizQuesList[i].answerStatus!!.equals(false)){
//                                        mQuizPendingList.add(mAllQuizQuesList.get(i))
//                                    }}

                                        for (i in mAllQuizQuesList.indices) {
                                            if (mAllQuizQuesList[i].answerStatus!!.equals(false)) {
                                                posss = i
                                                break
                                            }
                                        }

                                        if (!mAllQuizQuesList.size.equals(mQuizReadOutList.size)) {
                                            val intent = Intent(mActivity, QuizActivity::class.java)
                                            intent.putExtra("quiz_questions_list", mQuizPendingList)
                                            intent.putExtra("related_pod_list", mRelatedPod)
                                            intent.putExtra("position", posss)
                                            intent.putExtra(
                                                "nug_id",
                                                mNuggetsList.get(position).nugId
                                            )
                                            startActivity(intent)
                                            overridePendingTransitionSlideUPEnter()
                                        }
                                    }
                                }
                            } else {
                                isNugId = false
                                getReadStatusRequest()
                            }
                        }
                    }

//                }
            }

                    private fun performDrawerClick() {
                startActivity(Intent(mActivity, HomeActivity::class.java))
                overridePendingTransition(
                    R.anim.right_to_left,
                    R.anim.left_to_right
                )
            }

                    // - - Execute Save Data Api
                    private fun executeSaveDataRequest() {
                showProgressDialog(mActivity)
                saveViewModel?.saveData(mActivity, getAccessToken(), nug_id.toString(), "nugget")
                    ?.observe(this,
                        androidx.lifecycle.Observer<SaveModel?> { mModel ->
                            when (mModel.status) {
                                1 -> {
                                    dismissProgressDialog()
                                    showTickDialog(mActivity)
                                    imgSaveIV.setImageResource(R.drawable.ic_bookmark)
                                    mNuggetsList.get(position).nugSaveStatus = true
//                                    showToast(mActivity, mModel.message)
                                }
                                else -> {
                                    showAlertDialog(mActivity, mModel.message)
                                    dismissProgressDialog()
                                }
                            }
                        })
            }


    /*
     *
     * Error Alert Dialog
     * */
    fun showTickDialog(mActivity: Activity?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.tick_dialog)
        alertDialog.setCanceledOnTouchOutside(true)
        alertDialog.setCancelable(true)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val check = alertDialog.findViewById<CheckView>(R.id.check)
        check.check()
        imgHeaderIV.postDelayed(Runnable {
            alertDialog.dismiss()
        }, 2000)
        alertDialog.show()
    }

                    // - - Execute UnSave Data Api
                    private fun executeUnSaveDataRequest() {
                showProgressDialog(mActivity)
                unSaveViewModel?.unSaveData(
                    mActivity,
                    getAccessToken(),
                    nug_id.toString(),
                    "nugget"
                )
                    ?.observe(this,
                        androidx.lifecycle.Observer<UnSaveModel?> { mModel ->
                            when (mModel.status) {
                                1 -> {
                                    dismissProgressDialog()
                                    imgSaveIV.setImageResource(R.drawable.ic_save)
                                    mNuggetsList.get(position).nugSaveStatus = false
                                    showToast(mActivity, mModel.message)
                                }
                                else -> {
                                    showAlertDialog(mActivity, mModel.message)
                                    dismissProgressDialog()
                                }
                            }
                        })
            }

                    override fun onBackPressed() {
                super.onBackPressed()
                finishAffinity()
            }


                    /*
                    * Share Image Funcitonality:
                    *
                    * */

                    private fun performShareClick() {
                if (checkPermission()) {
                    takeScreenShotOfLayout()
                } else {
                    requestPermission()
                }
            }


            fun saveImage(finalBitmap: Bitmap, resourceName: String, context: Context) {
                val filename = resourceName

                //Output stream
                var fos: OutputStream? = null

                //For devices running android >= Q
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    //getting the contentResolver
                    context.contentResolver?.also { resolver ->

                        //Content resolver will process the contentvalues
                        val contentValues = ContentValues().apply {

                            //putting file information in content values
                            put(MediaStore.MediaColumns.DISPLAY_NAME, filename)
                            put(MediaStore.MediaColumns.MIME_TYPE, "image/png")
                            put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_DCIM)
                        }

                        //Inserting the contentValues to contentResolver and getting the Uri
                        val imageUri: Uri? =
                            resolver.insert(
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                contentValues
                            )
//                val sdcard = Environment.getExternalStorageDirectory()
//                if (sdcard != null) {
//                    val mediaDir = File(sdcard, "DCIM/Camera")
//                    if (!mediaDir.exists()) {
//                        mediaDir.mkdirs()
//                    }
//                }
                        imagePathh = imageUri
//                resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)
                        //Opening an outputstream with the Uri that we got
                        fos = imageUri?.let { resolver.openOutputStream(it) }
                    }
                } else {
                    //These for devices running on android < Q
                    //So I don't think an explanation is needed here
                    val imagesDir =
                        Utils.outputPath
                    val image = File(imagesDir, filename)
                    imagePathh = image.toUri()
                    fos = FileOutputStream(image)
                }

                fos?.use {
                    //Finally writing the bitmap to the output stream that we opened
                    finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, it)
//           context?.showT("Saved to Photos")
                }
//        val root = outputPath + resourceName
//        try {
//            val bos = ByteArrayOutputStream()
//            finalBitmap.compress(CompressFormat.JPEG, 100 /*ignored for PNG*/, bos)
//            val bitmapdata = bos.toByteArray()
//            val bs = ByteArrayInputStream(bitmapdata)
//
//            bs.toFile(root)
//            bos.flush()
//            bos.close()
//        } catch (e: java.lang.Exception) {
//            e.printStackTrace()
//        }
            }

                    private fun takeScreenShotOfLayout() {
                imgHeaderIV.visibility = View.VISIBLE
                shareLL.visibility = View.GONE
                txtRelatedListeningTV.visibility = View.GONE
                val bitmap = getScreenShotFromView(screenshotLL)
                // if bitmap is not null then
                // save it to gallery
                if (bitmap != null) {
                    saveImage(bitmap, "screenshot.png", mActivity)
                    val share = Intent(Intent.ACTION_SEND)
                    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
                    share.type = "image/*"
                    share.putExtra(Intent.EXTRA_STREAM, imagePathh)
                    startActivity(Intent.createChooser(share, "Share Image"))
                } else {
                    showToast(mActivity, getString(R.string.some_thing))
                }

            }


                    private fun getScreenShotFromView(v: View): Bitmap? {
                // create a bitmap object
                var screenshot: Bitmap? = null
                try {
                    // inflate screenshot object
                    // with Bitmap.createBitmap it
                    // requires three parameters
                    // width and height of the view and
                    // the background color
                    screenshot =
                        Bitmap.createBitmap(
                            v.measuredWidth,
                            v.measuredHeight,
                            Bitmap.Config.ARGB_8888
                        )
                    // Now draw this bitmap on a canvas
                    val canvas = Canvas(screenshot)
                    v.draw(canvas)

                } catch (e: Exception) {
                    Log.e("GFG", "Failed to capture screenshot because:" + e.message)
                }

                imgHeaderIV.visibility = View.GONE
                shareLL.visibility = View.VISIBLE
                if (relatedVisibility == true) {
                    txtRelatedListeningTV.visibility = View.VISIBLE
                } else {
                    txtRelatedListeningTV.visibility = View.GONE
                }
                // return the bitmap
                return screenshot
            }


                    private fun checkPermission(): Boolean {
                val write = ContextCompat.checkSelfPermission(this, writeExternalStorage)
                val read = ContextCompat.checkSelfPermission(this, writeReadStorage)
                return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED
            }

                    private fun requestPermission() {
                ActivityCompat.requestPermissions(
                    mActivity,
                    arrayOf(writeExternalStorage, writeReadStorage),
                    REQUEST_PERMISSION_CODE
                )
            }


                    override fun onRequestPermissionsResult(
                requestCode: Int,
                permissions: Array<String>,
                grantResults: IntArray
            ) {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
                when (requestCode) {
                    REQUEST_PERMISSION_CODE -> {
                        if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                            Log.e(TAG, "Permission has been denied by user")
                        } else {
                            //Perform Location Click Code
                            takeScreenShotOfLayout()
                        }
                    }
                }
            }

    override fun onDestroy() {
        super.onDestroy()
//        Toast.makeText(this, "Callback"+"destroyMyProgram", Toast.LENGTH_SHORT).show()
        end_tme = System.currentTimeMillis()
        Log.e(TAG, "onResponse1111: "+AppPreference().readString(mActivity, CHECKIN, ""))
//        Log.e(TAG, "end_time"+totall_time )
        AppPreference().writeLong(mActivity, END_MIN_SPENT, end_tme)
        AppPreference().writeString(mActivity, "destroy", "true")

//        start_time=TimeUnit.MILLISECONDS.toSeconds(AppPreference().readLong(
//            mActivity,
//            STARTT_TIME_SPENT,
//            0
//        ))
//        end_seconds=TimeUnit.MILLISECONDS.toSeconds(end_tme)
        totall_time=(((end_tme-AppPreference().readLong(
            mActivity,
            STARTT_TIME_SPENT,
            0
        ))/ 1000) % 60)

//        total_time =
//            AppPreference().readLong(mActivity, END_MIN_SPENT, end_tme) - AppPreference().readLong(
//                mActivity,
//                STARTT_TIME_SPENT,
//                0
//            )
//        totall_time=  TimeUnit.MILLISECONDS.toMinutes(total_time)
        Log.e(TAG, "total_time"+totall_time )
//        totall_time = (((total_time / (1000))/60) % 60)
        AppPreference().writeLong(mActivity, TOTAL_TIME_SPENT, totall_time)

    }

    override fun compare(p0: UsageStats?, p1: UsageStats?): Int {
        TODO("Not yet implemented")
    }




}

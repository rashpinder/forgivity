package com.forgivity.app.views.activity

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.forgivity.app.R
import com.forgivity.app.util.AppPreference
import kotlinx.android.synthetic.main.activity_congrats.*

class CongratsActivity : BaseActivity() {
//    var message: String? = ""
    var day_no: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_congrats)
        ButterKnife.bind(this)
//        Glide.with(this).load(R.drawable.square_cuts).into(gifIV)
        Glide.with(this).load(R.drawable.congrats).into(gifIV)
//        congratsIV.startAnimation(AnimationUtils.loadAnimation(this, R.anim.continuous_zoom_in_zoom_out))
        getIntentData()
    }

    private fun getIntentData() {
        if (intent != null) {
//            message = intent.getStringExtra("message")
            day_no = intent.getStringExtra("day_no")
            if (!day_no.equals(null) && !day_no.equals("")){
//                txtDayCompletedTV.text=resources.getString(R.string.day_comp)
            txtDayCompletedTV.text = "Congratulations! You have just\ncompleted Day $day_no. You are on your way\nin practice of Forgiveness. Keep up\nthe great work."
                txtCongratulationsTV.isVisible=false
        }
        else{
                txtCongratulationsTV.isVisible=true
                txtDayCompletedTV.text=resources.getString(R.string.you_did_it)

            }
        }
    }

    @OnClick(
        R.id.txtContinueTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtContinueTV -> performContinueClick()
        }
    }

    private fun performContinueClick() {
//        finish()
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        AppPreference().writeString(mActivity, "Congrats","true")
        finish()
    }

}
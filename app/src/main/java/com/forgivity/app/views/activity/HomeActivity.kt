package com.forgivity.app.views.activity

import android.app.Dialog
import android.app.TaskStackBuilder
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.TextView
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.util.*
import kotlinx.android.synthetic.main.activity_home.*
import java.util.*
import java.util.concurrent.TimeUnit

class HomeActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        ButterKnife.bind(this)
        mainLL.setBackgroundResource(R.drawable.navigation_drawer_bg)
    }

    private fun setDataOnWidgets() {
        profileNameTV.text = getName()
        when {
            getPersonType() == "EMOTIONAL" -> {
                profileImageIV.setBackgroundResource(R.drawable.ic_red_heart)
            }
            getPersonType() == "DECISIONAL" -> {
                profileImageIV.setBackgroundResource(R.drawable.ic_brain)
            }
            getPersonType() == "CONDITIONAL" -> {
                profileImageIV.setBackgroundResource(R.drawable.ic_flowers)
            }
            getPersonType() == "DOLLY LAMA" -> {
                profileImageIV.setBackgroundResource(R.drawable.ic_dolly_lama)
            }
            getPersonType() == "GRUDGE GANGSTA" -> {
                profileImageIV.setBackgroundResource(R.drawable.ic_ghost)
            }
            getPersonType() == "" -> {
                profileImageIV.setBackgroundResource(R.drawable.ic_app)
            }
        }
    }

    @OnClick(
        R.id.myProgramLL,
        R.id.myProgressLL,
        R.id.glossaryLL,
        R.id.helpLL,
        R.id.savedLL,
        R.id.settingsLL,
        R.id.activityFeedLL,
        R.id.exploreLL,
        R.id.termsConditionsLL,
        R.id.privacyPolicyLL,
        R.id.logOutLL
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.myProgramLL -> performMyProgramClick()
            R.id.myProgressLL -> performDashBoardClick()
            R.id.glossaryLL -> performGlossaryClick()
            R.id.helpLL -> performHelpClick()
            R.id.savedLL -> performSavedClick()
            R.id.settingsLL -> performSettingsClick()
            R.id.activityFeedLL -> performFeedClick()
            R.id.exploreLL -> performExploreClick()
            R.id.termsConditionsLL -> performTermsConditionsClick()
            R.id.privacyPolicyLL -> performPrivacyPolicyClick()
            R.id.logOutLL -> performLogOutClick()
        }
    }

    private fun performPrivacyPolicyClick() {
        val i = Intent(mActivity, WebViewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_PP)
        startActivity(i)
    }

    private fun performTermsConditionsClick() {
        val i = Intent(mActivity, WebViewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_TERMS)
        startActivity(i)
    }

    private fun performLogOutClick() {
        showLogoutConfirmAlertDialog()
    }

    private fun performExploreClick() {
        startActivity(Intent(mActivity, ExploreActivity::class.java))
    }

    private fun performMyProgramClick() {
        startActivity(Intent(mActivity, MyProgramActivity::class.java))
    }

    private fun performSettingsClick() {
        startActivity(Intent(mActivity, SettingsActivity::class.java))
    }

    private fun performFeedClick() {
        startActivity(Intent(mActivity, FeedActivity::class.java))
    }

    private fun performDashBoardClick() {
        startActivity(Intent(mActivity, DashboardActivity::class.java))
    }

    private fun performGlossaryClick() {
        startActivity(Intent(mActivity, GlossaryActivity::class.java))
    }

    private fun performHelpClick() {
        startActivity(Intent(mActivity, HelpActivity::class.java))
    }

    private fun performSavedClick() {
        preventMultipleClick()
        startActivity(Intent(mActivity, SavedActivity::class.java))
    }

    /*
       *
       * Logout Alert Dialog
       * */
    private fun showLogoutConfirmAlertDialog() {
        val alertDialog = mActivity.let { Dialog(it) }
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_logout)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val btnCancel = alertDialog.findViewById<TextView>(R.id.btnCancel)
        val btnLogout = alertDialog.findViewById<TextView>(R.id.btnLogout)
        btnCancel.setOnClickListener {
            alertDialog.dismiss()
        }
        btnLogout.setOnClickListener {
            alertDialog.dismiss()
            clearSharedPreferenceData()
        }
        alertDialog.show()
    }

    private fun clearSharedPreferenceData() {
        val preferences: SharedPreferences = AppPreference().getPreferences(applicationContext)
        val editor = preferences.edit()
        editor.remove(IS_LOGIN)
        editor.remove(ACCESS_TOKEN)
        editor.remove(NAME)
        editor.remove(AGE)
        editor.remove(GENDER)
        editor.remove(PERSON_TYPE)
        editor.remove(PROFILE_IMAGE)
        editor.remove(START_TIME_SPENT)
        editor.remove(STARTT_TIME_SPENT)
        editor.remove(END_TIME_SPENT)
        editor.remove(END_MIN_SPENT)
        editor.remove(TOTAL_TIME_SPENT)
        editor.remove(TOTALL_TIME_SPENT)
        editor.remove(NEW_STARTT_TIME_SPENT)
        editor.clear()
        editor.apply()
        editor.commit()
        // Intent
        val mIntent = Intent(mActivity, StartActivity::class.java)
        mActivity.startActivity(intent)
        TaskStackBuilder.create(mActivity).addNextIntentWithParentStack(mIntent).startActivities()
        mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP // To clean up all activities
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        mActivity.startActivity(mIntent)
        mActivity.finish()
        mActivity.finishAffinity()
    }

    override fun onResume() {
        super.onResume()
        setDataOnWidgets()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(mActivity, MyProgramActivity::class.java))
        overridePendingTransition(R.anim.right_to_left, R.anim.right_to_left)
        finishAffinity()
    }


    override fun onDestroy() {
//        end_tme = System.currentTimeMillis()
//        Log.e(TAG, "onResponse1111: "+AppPreference().readString(mActivity, CHECKIN, "") )
////        Log.e(TAG, "end_time"+totall_time )
//        AppPreference().writeLong(mActivity, END_MIN_SPENT, end_tme)
//
////        start_time=TimeUnit.MILLISECONDS.toSeconds(AppPreference().readLong(
////            mActivity,
////            STARTT_TIME_SPENT,
////            0
////        ))
////        end_seconds=TimeUnit.MILLISECONDS.toSeconds(end_tme)
//        totall_time=((end_tme-AppPreference().readLong(
//            mActivity,
//            STARTT_TIME_SPENT,
//            0
//        )) / 1000) % 60
//
////        total_time =
////            AppPreference().readLong(mActivity, END_MIN_SPENT, end_tme) - AppPreference().readLong(
////                mActivity,
////                STARTT_TIME_SPENT,
////                0
////            )
////        totall_time=  TimeUnit.MILLISECONDS.toMinutes(total_time)
//        Log.e(TAG, "total_time"+totall_time )
////        totall_time = (((total_time / (1000))/60) % 60)
//        AppPreference().writeLong(mActivity, TOTAL_TIME_SPENT, totall_time)
        super.onDestroy()

    }
}


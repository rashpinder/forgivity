package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.forgivity.app.R
import com.forgivity.app.model.StatusMsgModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.viewModel.GetReadingViewModel
import kotlinx.android.synthetic.main.activity_congrats.*
import kotlinx.android.synthetic.main.activity_congrats.txtDayCompletedTV
import kotlinx.android.synthetic.main.activity_my_program.*
import kotlinx.android.synthetic.main.activity_new_day.*
import kotlinx.android.synthetic.main.activity_new_day.txtDayTV
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewDayActivity : BaseActivity() {
    var day_no: String? = ""
    var day_desc: String? = ""
    var day_id: String? = ""
    var nug_id: String? = ""
    private var mReadingViewModel: GetReadingViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_day)
        ButterKnife.bind(this)
        mReadingViewModel = ViewModelProviders.of(this).get(GetReadingViewModel::class.java)
        getIntentData()
    }

    private fun getIntentData() {
        if (intent != null) {
            day_no = intent.getStringExtra("day_no")
            day_id = intent.getStringExtra("day_id")
            day_desc = intent.getStringExtra("day_desc")
            nug_id = intent.getStringExtra("nug_id")
            txtDayTV.text = "Day " + day_no
//            txtTitleTV.text = day_desc
        }
    }

    @OnClick(
        R.id.txtBeginTV,
        R.id.txtBackTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtBeginTV -> performBeginClick()
            R.id.txtBackTV -> performBackdayClick()
        }
    }

    private fun performBeginClick() {
        getReadStatussRequest()
//        val i = Intent(mActivity, MyProgramActivity::class.java)
//        overridePendingTransition(R.anim.rotate_out,R.anim.rotate_in)
//        startActivity(i)
    }



    private fun getReadStatussRequest() {
        if (!isNetworkAvailable(mActivity)) {
            imgHeaderIV.visibility = View.GONE
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeGetReadinggStatusRequest()
        }
    }


    // - - Execute UnSave Data Api
    private fun executeGetReadinggStatusRequest() {
        showProgressDialog(mActivity)
        day_id?.let {
            nug_id?.let { it1 ->
                mReadingViewModel?.getReadStatusRequest(mActivity, getAccessToken(), it, it1)
                    ?.observe(this,
                        androidx.lifecycle.Observer<StatusMsgModel?> { mModel ->
                            when (mModel.status) {
                                1 -> {
                                    dismissProgressDialog()
                                    finish()
                                }
                                else -> {
                                    showAlertDialog(mActivity, mModel.message)
                                    dismissProgressDialog()
                                }
                            }
                        })
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }


    private fun performBackdayClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeUnsetApi()
        }
    }

    private fun executeUnsetApi() {
        showProgressDialog(mActivity)
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.unsetReadStatusRequest(getAccessToken(), day_id!!).enqueue(object :
            Callback<StatusMsgModel?> {
            override fun onResponse(
                call: Call<StatusMsgModel?>?,
                response: Response<StatusMsgModel?>
            ) {
//                    dismissProgressDialog()
                val mModel: StatusMsgModel? = response.body()
                if (mModel?.status == 1) {
                   finish()
                } else {
                    dismissProgressDialog()
                    showToast(mActivity, mModel?.message)
                }
            }

            override fun onFailure(call: Call<StatusMsgModel?>?, t: Throwable?) {
                dismissProgressDialog()
                Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
            }
        })
    }


}
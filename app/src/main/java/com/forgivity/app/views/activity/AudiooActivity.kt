package com.forgivity.app.views.activity

import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.StrictMode
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.View.OnTouchListener
import android.widget.SeekBar
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.model.*
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.exoplayer2.source.MediaSourceFactory
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import io.branch.indexing.BranchUniversalObject
import io.branch.referral.Branch
import io.branch.referral.util.ContentMetadata
import io.branch.referral.util.LinkProperties
import kotlinx.android.synthetic.main.activity_audio.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AudiooActivity : BaseActivity() {

    // - - Initialize Objects
    private var isPlaying = true
    private var audioFile: String = ""
    private var pod_desc: String = ""
    private var pod_file: String = ""
    private var pod_type: String = ""
    private var pod_id: String = ""
    private var pod_title: String = ""
    private var type: String = ""
    private var value: String = ""
    private var audioFileName: String = ""
    private val myHandler = Handler()
    var mRelatedPod: RelatedPod? = null
    private var player: SimpleExoPlayer? = null
    private var audioManager: AudioManager? = null
    var mNuggetsList: ArrayList<RelatedNuggetsItem> = ArrayList()
    var mNuggetsReadOutList: ArrayList<String> = ArrayList()
    var day_id: String? = ""
    var day_no: String? = ""
    var day_desc: String? = ""
    var day_level: String? = ""
    var day_desc_read: Int? = 0
    var nug_id: String? = ""
    var mType: String? = ""
    var linked_id: String? = ""
    var mLinkedList: LinkedDetails? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_audio)
        ButterKnife.bind(this)
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                val m =
                    StrictMode::class.java.getMethod("disableDeathOnFileUriExposure")
                m.invoke(null)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
        volumeControlStream = AudioManager.STREAM_MUSIC
        getIntentData()
    }

    private fun initAudioControls() {
        try {
            audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
            audioSB!!.max = audioManager!!
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC)
            audioSB!!.progress = audioManager!!
                .getStreamVolume(AudioManager.STREAM_MUSIC)
            audioSB!!.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onStopTrackingTouch(arg0: SeekBar) {}
                override fun onStartTrackingTouch(arg0: SeekBar) {}
                override fun onProgressChanged(arg0: SeekBar, progress: Int, arg2: Boolean) {
                    audioManager!!.setStreamVolume(
                        AudioManager.STREAM_MUSIC,
                        progress, AudioManager.FLAG_PLAY_SOUND
                    )
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private val focusChangeListener = AudioManager.OnAudioFocusChangeListener { focusChange ->
        try {
            val mediaPlayerBackground = MediaPlayer()

            when (focusChange) {
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK ->                                 // Lower the volume while ducking.
                {
                    mediaPlayerBackground.setVolume(0.2f, 0.2f)
                    pauseAudio()
                }
                AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> {
                    mediaPlayerBackground.stop()
                    pauseAudio()
                }
                AudioManager.AUDIOFOCUS_LOSS -> {
                    mediaPlayerBackground.stop()
                    pauseAudio()
                }
                AudioManager.AUDIOFOCUS_GAIN -> {
                    // Return the volume to normal and resume if paused.
                    mediaPlayerBackground.setVolume(1f, 1f)
                    mediaPlayerBackground.start()
                }
                else -> {
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    fun initializePlayer(audio: String) {
        try {
            releasePlayer()
            val mediaDataSourceFactory: DataSource.Factory =
                DefaultDataSourceFactory(
                    mActivity,
                    Util.getUserAgent(mActivity, "LettersSample"),
                    null
                )

            val mediaSource = ProgressiveMediaSource.Factory(mediaDataSourceFactory)
                .createMediaSource(MediaItem.fromUri(audio))

            val mediaSourceFactory: MediaSourceFactory =
                DefaultMediaSourceFactory(mediaDataSourceFactory)

            player = SimpleExoPlayer.Builder(mActivity)
                .setMediaSourceFactory(mediaSourceFactory)
                .build()

            player!!.addMediaSource(mediaSource)
            player!!.prepare()
            player!!.repeatMode = SimpleExoPlayer.REPEAT_MODE_ONE
            player!!.playWhenReady = true
            audioTimer()

            val ama = getSystemService(AUDIO_SERVICE) as AudioManager
            // Request audio focus for playback
            val result = ama.requestAudioFocus(
                focusChangeListener,  // Use the music stream.
                AudioManager.STREAM_MUSIC,  // Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN
            )
            initAudioControls()
            if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                // other app had stopped playing song now , so u can do your stuff now .
            }
        } catch (e: Exception) {
            Log.e("ExoPlayerFeaturedTab", e.message.toString())
        }
    }


    fun releasePlayer() {
        if (player != null) {
            player!!.playWhenReady = false
            player!!.stop()
            player!!.release()
            player = null
        }
    }


    private fun getIntentData() {
        if (intent != null) {
            linked_id = intent.getIntExtra("linked_id",0).toString()
            mType = intent.getStringExtra("mType")
            mRelatedPod = intent.getParcelableExtra("list")
            value = intent.getStringExtra("value").toString()
            audioFile = mRelatedPod?.podFile.toString()
            type = mRelatedPod?.podType.toString()
            audioFileName = mRelatedPod?.podDisplayTitle.toString()
            txtAudioTitleTV.text = mRelatedPod?.podDisplayTitle
            pod_file = intent.getStringExtra("pod_file").toString()
                pod_id = intent.getStringExtra("pod_id").toString()
                pod_title = intent.getStringExtra("pod_title").toString()
                pod_desc = intent.getStringExtra("pod_desc").toString()
                pod_type = intent.getStringExtra("pod_type").toString()

            if (!pod_file.equals("null") && !pod_file.equals("")) {
                audioFile = pod_file
                type = pod_type
                audioFileName = pod_title
                txtAudioTitleTV.text = pod_title
                initializePlayer(audioFile)
            }
            if (!mType.equals("")&& !mType.equals(null)){
                getLinkedDetails()
            }}
        if ((!mType.equals("")&& !mType.equals(null))||(!pod_file.equals("null") && !pod_file.equals(""))) {
//        initializePlayer(audioFile)
        }
        else{
            initializePlayer(audioFile)
        }
    }


    private fun getLinkedDetails() {
        if (!isNetworkAvailable(this)) {
            showAlertDialog(this, getString(R.string.internet_connection_error))
        } else {
            executeLinkedApi()
        }
    }

    private fun executeLinkedApi() {
        showProgressDialog(this)
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getLinkedDetailsRequest(getAccessToken(),
            linked_id.toString(), mType.toString()
        )
            .enqueue(object :
                Callback<LinkedDetailsModel?> {
                override fun onResponse(
                    call: Call<LinkedDetailsModel?>?,
                    response: Response<LinkedDetailsModel?>
                ) {
                    dismissProgressDialog()
                    val mModel: LinkedDetailsModel? = response.body()
                    if (mModel?.status == 1) {
                        mLinkedList = mModel.linked_details
                        audioFile = mLinkedList!!.pod_file
                        type = mLinkedList!!.pod_type
                        audioFileName = mLinkedList!!.pod_display_title
                        txtAudioTitleTV.text = mLinkedList!!.pod_display_title
                        initializePlayer(audioFile)
                    }
                    else {
                        dismissProgressDialog()
                    }
                }

                override fun onFailure(call: Call<LinkedDetailsModel?>?, t: Throwable?) {
                    dismissProgressDialog()
                    Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
                }
            })}



    @OnClick(
        R.id.imgCrossIV,
        R.id.imgPlayPauseAudioIV,
        R.id.imgBackwardIV,
        R.id.imgForwardIV,
        R.id.imgShareIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgCrossIV -> onBackPressed()
            R.id.imgPlayPauseAudioIV -> performPlayPauseAudioClick()
            R.id.imgBackwardIV -> performBackwardAudioClick()
            R.id.imgForwardIV -> performForwardAudioClick()
            R.id.imgShareIV -> performShareAudioClick(type, audioFileName, audioFile)
        }
    }


    private fun performShareAudioClick(
        type: String,
        title: String?,
        podFile: String
    ) {

        val buo = BranchUniversalObject()
            .setCanonicalIdentifier("content/12345")
            .setTitle("Forgivity")
            .setContentImageUrl("https://forgivityapp.com/v2/uploads/ic_splash.png")
            .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
            .setLocalIndexMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC)
            .setContentMetadata(
                ContentMetadata().addCustomMetadata("link", podFile).addCustomMetadata(
                    "type",
                    type
                )
            )

        val lp = LinkProperties()
            .setFeature("sharing")
            .setCampaign("content_sharing")
            .setStage("new user")

        buo.generateShortUrl(
            mActivity, lp,
            Branch.BranchLinkCreateListener { url, error ->
                if (error == null) {
// scanGallery(context, pictureFile.absolutePath, downUri.toString())
                    shareData(url)
                    Log.e("Checki", "3")
                } else {
                    var url1 = ""
                    Log.e("Checki", "4")
                }
            })
    }


    fun shareData(url: String) {
        Log.e("Checki", "5")
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, url)
            type = "text/plain"
        }
        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
    }


    private fun performForwardAudioClick() {
        val currentAudioDuration = player!!.currentPosition + 5000
        player!!.seekTo(currentAudioDuration.toInt().toLong())
    }

    private fun performBackwardAudioClick() {
        val currentAudioDuration = player!!.currentPosition - 5000
        if (audioTimerSB.progress != 0 && audioTimerSB.progress > 5) {
            player!!.seekTo(currentAudioDuration.toInt().toLong())
        } else {
            showToast(mActivity, "progress value error")
        }
    }

    private fun audioTimer() {
        audioTimerSB.progress = 0
        audioTimerSB.max = 100
        audioTimerSB.setOnTouchListener(OnTouchListener { v, event -> true })
        updateProgressBar()
    }

    //Audio player functions
    private fun updateProgressBar() {
        myHandler.postDelayed(UpdateTimeTask, 100)
    }

    private val UpdateTimeTask: Runnable = object : Runnable {
        override fun run() {
            if (player != null) {
                val totalduration = player?.duration
                val currentduration = player?.currentPosition
                startTimerTV.text = "" + currentduration?.let { milliSecondstoTimer(it) }
                endTimerTV.text = "" + totalduration?.let { milliSecondstoTimer(it) }
                Handler().postDelayed({
                    endTimerTV.visibility = View.VISIBLE
                }, 2000)

                audioTimerSB!!.progress = getProgressPercentage(
                    currentduration!!,
                    totalduration!!
                )
                myHandler.postDelayed(this, 100)
            }
        }
    }

    fun milliSecondstoTimer(milliseconds: Long): String? {
        var finalTimerString = ""
        var secondsString = ""
        var minuteString = ""
        val hours = (milliseconds / (1000 * 60 * 60)).toInt()
        val minutes = (milliseconds % (1000 * 60 * 60)).toInt() / (1000 * 60)
        val seconds = (milliseconds % (1000 * 60 * 60) % (1000 * 60) / 1000).toInt()
        if (hours > 0) {
            finalTimerString = if (hours < 10) {
                "0$hours:"
            } else "" + hours
        }
        secondsString = if (seconds < 10) {
            "0$seconds"
        } else "" + seconds

        minuteString = if (minutes < 10) {
            "0$minutes"
        } else "" + minutes
        finalTimerString = "$finalTimerString$minuteString:$secondsString"
        return finalTimerString
    }

    fun getProgressPercentage(currentduration: Long, totalduration: Long): Int {
        var percentage = 0.toDouble()
        val currentseconds = currentduration.toInt() / 1000.toLong()
        val totalseconds = totalduration.toInt() / 1000.toLong()
        percentage = currentseconds.toDouble() / totalseconds * 100
        return percentage.toInt()
    }

    override fun onPause() {
        super.onPause()
        pause_previous_player()
//        releasePlayer()
    }


    fun pause_previous_player() {
        if (player != null) {
            player!!.playWhenReady = false
        }
    }

    override fun onStop() {
        super.onStop()
//        releasePlayer()
    }

    override fun onDestroy() {
        super.onDestroy()
        releasePlayer()
    }

    override fun onResume() {
        super.onResume()
        start_Previous_Player()
    }


    fun start_Previous_Player() {
        if (player != null) {
            player!!.playWhenReady = true
        }
    }

    private fun performPlayPauseAudioClick() {
        if (isPlaying) {
            pauseAudio()
        } else if (!isPlaying) {
            playAudio()
        }
    }

    private fun pauseAudio() {
        if (player != null && player!!.isPlaying) {
            imgPlayPauseAudioIV.setImageResource(R.drawable.ic_play_rl)
            player!!.playWhenReady = false
            player!!.playbackState
            isPlaying = false
        }
    }

    private fun playAudio() {
        player!!.playWhenReady = true
        player!!.playbackState
        isPlaying = true
        imgPlayPauseAudioIV.setImageResource(R.drawable.ic_pause_audio)
    }

    override fun onBackPressed() {
        if (value.equals("Thankyou")) {
            getReadingDetails()
        } else {
            super.onBackPressed()
        }
        pauseAudio()
    }


    private fun getReadingDetails() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeApi()
        }
    }

    private fun executeApi() {
        mNuggetsList.clear()
        mNuggetsReadOutList.clear()
        showProgressDialog(mActivity)
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getReadingRequest(getAccessToken()).enqueue(object :
            Callback<ReadingModel?> {
            override fun onResponse(
                call: Call<ReadingModel?>?,
                response: Response<ReadingModel?>
            ) {
                dismissProgressDialog()
                val mModel: ReadingModel? = response.body()
                if (mModel?.status == 1) {
                    mNuggetsList = mModel.dayDetail!!.relatedNuggets
                    day_id = mModel.dayDetail.dayId
                    day_no = mModel.dayDetail.dayNo
                    day_desc = mModel.dayDetail.dayDesc
                    day_level = mModel.dayDetail.dayLevel
                    day_desc_read = mModel.dayDetail.dayDescRead
                    for (i in mNuggetsList.indices) {
                        /*
                         to get readout list
                         */
                        if (mNuggetsList.get(i).readStatus == 1) {
                            mNuggetsReadOutList.add(mNuggetsList.get(i).toString())
                        }
                    }

                    if ((day_desc_read == 0) && (mNuggetsReadOutList.size == 0)) {
                        val i = Intent(mActivity, MyProgramActivity::class.java)
                        i.putExtra("day_no", day_no)
                        i.putExtra("day_desc", day_desc)
                        i.putExtra("day_id", day_id)
                        i.putExtra("nug_id", "0")
//                        overridePendingTransition(R.anim.rotate_out,R.anim.rotate_in)
                        startActivity(i)
                        finish()
                    }
//                    else{
//                        startActivity(Intent(mActivity, MyProgramActivity::class.java))
//                        finish()
//                    }
                } else {
                    showToast(mActivity, mModel?.message)
                }
            }

            override fun onFailure(call: Call<ReadingModel?>?, t: Throwable?) {
                dismissProgressDialog()
                Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
            }
        })
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        val audio = this.getSystemService(AUDIO_SERVICE) as AudioManager
        val currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC)
        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            audioSB.progress = currentVolume
            return false
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            audioSB.progress = currentVolume
            return false
        }
        return super.onKeyDown(keyCode, event)
    }

}
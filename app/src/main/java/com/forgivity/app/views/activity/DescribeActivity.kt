package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProviders
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.model.GetTokenModel
import com.forgivity.app.model.StatusMsgModel
import com.forgivity.app.model.UpdateProfileModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.util.*
import com.forgivity.app.viewModel.GetTokenViewModel
import com.forgivity.app.viewModel.UpdateProfileViewModel
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_describe.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class DescribeActivity : BaseActivity() {

    // - - Initialize Objects
    private var getTokenViewModel: GetTokenViewModel? = null
    private var updateProfileViewModel: UpdateProfileViewModel? = null
//    private var mOrgID: String = "1"
    var strDeviceToken: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_describe)
        ButterKnife.bind(this)
        getTokenViewModel = ViewModelProviders.of(this).get(GetTokenViewModel::class.java)
        updateProfileViewModel = ViewModelProviders.of(this).get(UpdateProfileViewModel::class.java)
        getDeviceToken()
        mAccessToken()
        userTypeSelection()
    }

    private fun getDeviceToken() {
        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new FCM registration token
                strDeviceToken = task.result!!
                AppPreference().writeString(
                    mActivity,
                    DEVICE_TOKEN,
                    strDeviceToken
                )
                Log.e(TAG, "**Push Token**$strDeviceToken")
                // Log and toast
                Log.d(TAG, "Token********   $strDeviceToken")
            })

    }

    private fun mAccessToken() {
        when {
            getAccessToken() == "" -> {
                executeGetTokenApi()
            }
        }
    }

    @OnClick(
        R.id.heartIV,
        R.id.imgBackIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> onBackPressed()
        }
    }

    private fun userTypeSelection() {
        emotionalRL.setOnClickListener {
            if (getPersonType().equals("")) {
            AppPreference().writeString(this, PERSON_TYPE, "EMOTIONAL")
            AppPreference().writeString(this, PROFILE_IMAGE, R.drawable.ic_red_heart.toString())
            performClick()
            } else {
                AppPreference().writeString(this, PERSON_TYPE, "EMOTIONAL")
                AppPreference().writeString(this, PROFILE_IMAGE, R.drawable.ic_red_heart.toString())
                onBackPressed()
            }
        }
        decisionalRL.setOnClickListener {
            if (getPersonType().equals("")) {
            AppPreference().writeString(this, PERSON_TYPE, "DECISIONAL")
            AppPreference().writeString(this, PROFILE_IMAGE, R.drawable.ic_brain.toString())
            performClick()
            } else {
                AppPreference().writeString(this, PERSON_TYPE, "DECISIONAL")
                AppPreference().writeString(this, PROFILE_IMAGE, R.drawable.ic_brain.toString())
                onBackPressed()
            }

        }

        conditionalRL.setOnClickListener {
            if (getPersonType().equals("")) {
            AppPreference().writeString(this, PERSON_TYPE, "CONDITIONAL")
            AppPreference().writeString(this, PROFILE_IMAGE, R.drawable.ic_flowers.toString())
            performClick()
            } else {
                AppPreference().writeString(this, PERSON_TYPE, "CONDITIONAL")
                AppPreference().writeString(this, PROFILE_IMAGE, R.drawable.ic_flowers.toString())
                onBackPressed()
            }
        }
        dollyLammaRL.setOnClickListener {
            if (getPersonType().equals("")) {
            AppPreference().writeString(this, PERSON_TYPE, "DOLLY LAMA")
            AppPreference().writeString(this, PROFILE_IMAGE, R.drawable.ic_dolly_lama.toString())
            performClick()
            } else {
                AppPreference().writeString(this, PERSON_TYPE, "DOLLY LAMA")
                AppPreference().writeString(this, PROFILE_IMAGE, R.drawable.ic_dolly_lama.toString())
                onBackPressed()
            }
        }
        grudgeGangstaRL.setOnClickListener {
            if (getPersonType().equals("")) {
            AppPreference().writeString(this, PERSON_TYPE, "GRUDGE GANGSTA")
            AppPreference().writeString(this, PROFILE_IMAGE, R.drawable.ic_ghost.toString())
            performClick()
            } else {
                AppPreference().writeString(this, PERSON_TYPE, "GRUDGE GANGSTA")
                AppPreference().writeString(this, PROFILE_IMAGE, R.drawable.ic_ghost.toString())
                onBackPressed()
            }
        }
    }

    private fun executeGetTokenApi() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeGetTokenApiRequest()
        }
    }

    private fun executeGetTokenApiRequest() {
        showProgressDialog(mActivity)
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getTokenRequest(
            AppPreference().readString(mActivity, ORG_ID,null).toString()
        )
            .enqueue(object :
                Callback<GetTokenModel?> {
                override fun onResponse(
                    call: Call<GetTokenModel?>?,
                    response: Response<GetTokenModel?>
                ) {
                    dismissProgressDialog()
                    val mModel: GetTokenModel? = response.body()
                    if (mModel?.status == 1) {
                        AppPreference().writeString(mActivity, ACCESS_TOKEN, mModel.accessToken)
                        AppPreference().writeBoolean(mActivity, IS_LOGIN, true)
                        Log.e("access_token", mModel.accessToken!!)
                    } else {
                        showAlertDialog(mActivity, mModel?.message)
                    }
                }

                override fun onFailure(call: Call<GetTokenModel?>?, t: Throwable?) {
                    Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
                }
            })
    }

//
//    // - - Execute Get Token Request
//    private fun executeGetTokenApi() {
//        showProgressDialog(mActivity)
//        getTokenViewModel?.getTokenData(mActivity,
//            AppPreference().readString(mActivity, ORG_ID,null).toString()
//        )?.observe(this,
//            androidx.lifecycle.Observer<GetTokenModel?> { mModel ->
//                when (mModel.status) {
//                    1 -> {
//                        dismissProgressDialog()
//                        AppPreference().writeString(mActivity, ACCESS_TOKEN, mModel.accessToken)
//                        AppPreference().writeBoolean(mActivity, IS_LOGIN, true)
//                        Log.e("access_token", mModel.accessToken!!)
//                    }
//                    else -> {
//                        dismissProgressDialog()
//                        showAlertDialog(mActivity, mModel.message)
//                    }
//                }
//            })
//    }

    private fun performClick() {
        if (isNetworkAvailable(mActivity)) {
            executeUpdateProfileApi()
        } else
            showToast(mActivity, getString(R.string.internal_server_error))
    }

    // - - Execute Update Profile Request
    private fun executeUpdateProfileApi() {
        showProgressDialog(mActivity)
        updateProfileViewModel?.updateProfileData(
            mActivity,
            AppPreference().readString(mActivity, ORG_ID,null).toString(),
            getName(),
            getAge(),
            getInterest(),
            getGender(),
            getPersonType(),
            getAccessToken(),
            strDeviceToken,
            "Android"
        )?.observe(this,
            androidx.lifecycle.Observer<UpdateProfileModel?> { mModel ->
                if (mModel.status == 1) {
                    dismissProgressDialog()
                    val intent = Intent(mActivity, WelcomeActivity1::class.java)
                    AppPreference().writeBoolean(mActivity, GOT_IT, true)
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
//                    finish()
                    finishAffinity()
                } else {
                    showAlertDialog(mActivity, mModel.message)
                    dismissProgressDialog()
                }
            })
    }
}
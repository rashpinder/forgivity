package com.forgivity.app.views.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.util.CONTACT_WEB_LINK
import com.forgivity.app.util.FAQ_WEB_LINK
import com.forgivity.app.util.LINK_TYPE

class HelpActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)
        ButterKnife.bind(this)
    }

    @OnClick(
        R.id.imgBackIV,
        R.id.txtFaqTV,
        R.id.txtContactTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgBackIV -> performBackClick()
            R.id.txtFaqTV -> performFaqClick()
            R.id.txtContactTV -> performContactClick()
        }
    }

    private fun performFaqClick() {
        val i = Intent(mActivity, WebViewActivity::class.java)
        i.putExtra(LINK_TYPE, FAQ_WEB_LINK)
        startActivity(i)
    }

    private fun performContactClick() {
        val i = Intent(mActivity, WebViewActivity::class.java)
        i.putExtra(LINK_TYPE, CONTACT_WEB_LINK)
        startActivity(i)
    }




    private fun performDrawerClick() {
        startActivity(Intent(mActivity, HomeActivity::class.java))
        overridePendingTransition(
            R.anim.fade_in,
            R.anim.fade_out
        )
    }

    private fun performBackClick() {
        startActivity(Intent(mActivity, MyProgramActivity::class.java))
        finish()
    }
}
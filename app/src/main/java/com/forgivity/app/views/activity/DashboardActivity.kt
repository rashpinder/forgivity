package com.forgivity.app.views.activity

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.OnClick
import com.forgivity.app.R
import com.forgivity.app.adapter.DaysAdapter
import com.forgivity.app.interfaces.DaysInterface
import com.forgivity.app.model.*
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.util.AppPreference
import com.forgivity.app.util.TOTALL_TIME_SPENT
import com.forgivity.app.viewModel.DashboardViewModel
import kotlinx.android.synthetic.main.activity_dashboard.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DashboardActivity : BaseActivity() {

    // - - Initialize Objects
    private var dashboardViewModel: DashboardViewModel? = null
    lateinit var mDaysAdapter: DaysAdapter
    lateinit var dayID: String
    var mList: ArrayList<String> = ArrayList()
    var mDaysList: ArrayList<String> = ArrayList()
    var mLetterList: ArrayList<String> = ArrayList()
//    var mGList: ArrayList<String> = ArrayList()
//    var mIList: ArrayList<String> = ArrayList()
//    var mVList: ArrayList<String> = ArrayList()
//    var mEList: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        ButterKnife.bind(this)
        dashboardViewModel = ViewModelProviders.of(this).get(DashboardViewModel::class.java)
        executeDashboardApi()
    }

    @OnClick(
        R.id.imgDrawer,
        R.id.daysCompletedLL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.imgDrawer -> performDrawerClick()
            R.id.daysCompletedLL -> performDaysCompletedClick()
        }
    }

    private fun performDaysCompletedClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeHistoryRequest()
        }
    }

    private fun executeHistoryRequest() {
        if (mDaysList!=null){
            mDaysList.clear()
            mLetterList.clear()
        }
        showProgressDialog(mActivity)
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getHistoryRequest(getAccessToken())
            .enqueue(object :
                Callback<HistoryModel?> {
                override fun onResponse(
                    call: Call<HistoryModel?>?,
                    response: Response<HistoryModel?>
                ) {
                    dismissProgressDialog()
                    val mModel: HistoryModel? = response.body()
                    if (mModel?.status == 1) {
                        if (mModel.all_levels.F!=null){
                            mDaysList.addAll(mModel.all_levels.F)
                            for (i in 1..mModel.all_levels.F.size){
                            mLetterList.add("F")}
                        }
                        Log.e(TAG,"letter"+mLetterList.size)
                        if (mModel.all_levels.O!=null){
                            mDaysList.addAll(mModel.all_levels.O)
                            for (i in 1..mModel.all_levels.O.size){
                                mLetterList.add("O")}
                        }
                        if (mModel.all_levels.R!=null){
                            mDaysList.addAll(mModel.all_levels.R)
                            for (i in 1..mModel.all_levels.R.size){
                                mLetterList.add("R")}
                        }
                        if (mModel.all_levels.G!=null){
                            mDaysList.addAll(mModel.all_levels.G)
                            for (i in 1..mModel.all_levels.G.size){
                                mLetterList.add("G")}
                        }
                        if (mModel.all_levels.I!=null){
                            mDaysList.addAll(mModel.all_levels.I)
                            for (i in 1..mModel.all_levels.I.size){
                                mLetterList.add("I")}
                        }
                        if (mModel.all_levels.V!=null){
                            mDaysList.addAll(mModel.all_levels.V)
                            for (i in 1..mModel.all_levels.V.size){
                                mLetterList.add("V")}
                        }
                        if (mModel.all_levels.E!=null){
                            mDaysList.addAll(mModel.all_levels.E)
                            for (i in 1..mModel.all_levels.E.size){
                                mLetterList.add("E")}
                        }
//                        for (i in mModel.all_levels.F.indices) {
////                            mDaysList.addAll(mModel.all_levels.F)
//                            mDaysList= mModel.all_levels.F as ArrayList<String>
//                        }}
//                        if (mModel.all_levels.O!=null){
//                        for (i in mModel.all_levels.O.indices) {
////                            mDaysList.addAll(mModel.all_levels.O)
//                            mDaysList= mModel.all_levels.O as ArrayList<String>
//                        }}
//                        if (mModel.all_levels.R!=null){
//                        for (i in mModel.all_levels.R.indices) {
//                            mDaysList.addAll(mModel.all_levels.R)
//                        }}
//                        if (mModel.all_levels.G!=null){
//                        for (i in mModel.all_levels.G.indices) {
//                            mDaysList.addAll(mModel.all_levels.G)
//                        }}
//                        if (mModel.all_levels.I!=null){
//                        for (i in mModel.all_levels.I.indices) {
//                            mDaysList.addAll(mModel.all_levels.I)
//                        }}
//                        if (mModel.all_levels.V!=null){
//                        for (i in mModel.all_levels.V.indices) {
//                            mDaysList.addAll(mModel.all_levels.V)
//                        }}
//                        if (mModel.all_levels.E!=null){
//                        for (i in mModel.all_levels.E.indices) {
//                            mDaysList.addAll(mModel.all_levels.E)
//                        }}

                        Log.e(TAG,"listttt"+mDaysList.size)
                       showNavigationDialog(mActivity,mDaysList)
                    } else {
                        dismissProgressDialog()
                        showToast(mActivity, mModel?.message)
                    }
                }

                override fun onFailure(call: Call<HistoryModel?>?, t: Throwable?) {
                    dismissProgressDialog()
                    Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
                }
            })
    }


    //click of options
    var mDaysClickListner: DaysInterface = object : DaysInterface {
        override fun onDaysClickListener(dayID: String) {
            this@DashboardActivity.dayID = dayID
        }
    }

    private fun performDrawerClick() {
        startActivity(Intent(mActivity, HomeActivity::class.java))
        overridePendingTransition(
            R.anim.right_to_left,
            R.anim.left_to_right
        )
    }

    // - - Execute DashBoard Request
    private fun executeDashboardApi() {
        showProgressDialog(mActivity)
        dashboardViewModel?.dashboardData(mActivity, getAccessToken())?.observe(this,
            androidx.lifecycle.Observer<DashboardModel?> { mModel ->
                if (mModel.status == 1) {
                    dismissProgressDialog()
                    setDataOnWidgets(mModel)
                } else {
                    showAlertDialog(mActivity, mModel.message)
                    dismissProgressDialog()
                }
            })
    }

    //      Set Data on Widgets
    private fun setDataOnWidgets(mModel: DashboardModel) {
//        for(i in 1..mModel.userDetail?.dayCompleted!!){
//            mDaysList.addAll(listOf(i.toString()))
//        }
        txtCheckInTV.text = mModel.userDetail!!.checkIn.toString()
//        if (AppPreference().readLong(mActivity,TOTALL_TIME_SPENT,0)>= mModel.userDetail.timeSpent!!.toLong()){
//        txtTimeSpentTV.setText(AppPreference().readLong(mActivity,TOTALL_TIME_SPENT,0).toString()) }
//        else{
            txtTimeSpentTV.text = mModel.userDetail.timeSpent.toString()
//        }
        txtSavedToolsTV.text = mModel.userDetail.savedTools.toString()
        txtSavedDaysTV.text = mModel.userDetail.savedDays.toString()
        txtDaysCompletedTV.text = mModel.userDetail.dayCompleted.toString()
        txtLevelCompletedTV.text = mModel.userDetail.levelCompleted.toString()
        if (mModel.userDetail.levelCompleted == 1) {
            imgLevelCompletedIV.setImageDrawable(resources.getDrawable(R.drawable.ic_p_one))
        } else if (mModel.userDetail.levelCompleted == 2) {
            imgLevelCompletedIV.setImageDrawable(resources.getDrawable(R.drawable.ic_p_two))
        } else if (mModel.userDetail.levelCompleted == 3) {
            imgLevelCompletedIV.setImageDrawable(resources.getDrawable(R.drawable.ic_p_three))
        } else if (mModel.userDetail.levelCompleted == 4) {
            imgLevelCompletedIV.setImageDrawable(resources.getDrawable(R.drawable.ic_p_four))
        } else if (mModel.userDetail.levelCompleted == 5) {
            imgLevelCompletedIV.setImageDrawable(resources.getDrawable(R.drawable.ic_p_five))
        } else if (mModel.userDetail.levelCompleted == 6) {
            imgLevelCompletedIV.setImageDrawable(resources.getDrawable(R.drawable.ic_p_six))
        } else if (mModel.userDetail.levelCompleted == 7) {
            imgLevelCompletedIV.setImageDrawable(resources.getDrawable(R.drawable.level_seven_e))
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right)
    }

    /*
*
* Correct Alert Dialog
* */
    fun showNavigationDialog(
        mActivity: Activity?,
        mDaysList: ArrayList<String>
    ) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.item_day_navigation_dialog)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtNavigateTV = alertDialog.findViewById<TextView>(R.id.txtNavigateTV)
        val daysRV = alertDialog.findViewById<RecyclerView>(R.id.daysRV)
        val txtCancelTV = alertDialog.findViewById<TextView>(R.id.txtCancelTV)
        mDaysAdapter = DaysAdapter(
            mActivity,
            mDaysList,
            mLetterList,
            mDaysList.size-1,
            mDaysClickListner
        )
        daysRV.layoutManager =
            LinearLayoutManager(
                mActivity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
        daysRV.adapter = mDaysAdapter

        txtCancelTV.setOnClickListener {
            alertDialog.dismiss()
        }

        txtNavigateTV.setOnClickListener {
            alertDialog.dismiss()
            performNavigateClick()
        }
        alertDialog.show()
    }

    private fun performNavigateClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            executeUnreadDayApiRequest()
        }
    }

    private fun executeUnreadDayApiRequest() {
        showProgressDialog(mActivity)
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.unreadDayRequest(getAccessToken(), dayID)
            .enqueue(object :
                Callback<StatusMsgModel?> {
                override fun onResponse(
                    call: Call<StatusMsgModel?>?,
                    response: Response<StatusMsgModel?>
                ) {
                    dismissProgressDialog()
                    val mModel: StatusMsgModel? = response.body()
                    if (mModel?.status == 1) {
                        val i = Intent(mActivity, MyProgramActivity::class.java)
                        startActivity(i)
                        finish()
                    } else {
                        dismissProgressDialog()
                        showToast(mActivity, mModel?.message)
                    }
                }

                override fun onFailure(call: Call<StatusMsgModel?>?, t: Throwable?) {
                    dismissProgressDialog()
                    Log.e("GetReadingViewModel", "******ERROR******${t.toString()}")
                }
            })
    }


}
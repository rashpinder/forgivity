package com.forgivity.app.adapter

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.forgivity.app.R
import com.forgivity.app.interfaces.LinkedDataInterface
import com.forgivity.app.interfaces.LoadMoreActivityFeedData
import com.forgivity.app.model.ListItemFeed
import com.forgivity.app.views.activity.AudioActivity
import com.forgivity.app.views.activity.AudiooActivity

class FeedAdapter(
    var mActivity: Activity, var mArrayList: ArrayList<ListItemFeed?>?,
    var mLoadMoreScrollListener: LoadMoreActivityFeedData? = null,
    var mLinkedDataListener: LinkedDataInterface? = null
) :
    RecyclerView.Adapter<FeedAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_activity_feed, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mModel: ListItemFeed? = mArrayList!![position]
        holder.notificationTitleTV.text = mArrayList?.get(position)?.notifyType
        holder.notificationDetailTV.text = mArrayList?.get(position)?.notifyMessage

        if (mArrayList!!.size % 10 == 0 && mArrayList!!.size - 1 == position) {
            if (mModel != null) {
                mLoadMoreScrollListener?.onLoadMoreActivityFeedData(mModel)
            }
        }

        holder.itemView.setOnClickListener {
            when (mArrayList?.get(position)?.notifyType) {
                "nugget" -> {
                    mLinkedDataListener?.onItemClickListener(mModel?.typeId.toString(),"nugget")
                }
                "podcast" -> {
                    mLinkedDataListener?.onItemClickListener(mModel?.typeId.toString(),"podcast")
                }
                "survey" -> {
                    mLinkedDataListener?.onItemClickListener(mModel?.typeId.toString(),"survey")
                }
                "mass" -> {
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return mArrayList?.size!!
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var notificationTitleTV: TextView = itemView.findViewById(R.id.notificationTitleTV)
        var notificationDetailTV: TextView = itemView.findViewById(R.id.notificationDetailTV)
    }


}
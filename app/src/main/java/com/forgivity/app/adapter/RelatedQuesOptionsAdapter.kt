package com.forgivity.app.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.forgivity.app.R
import com.forgivity.app.interfaces.OnItemClickListener
import com.forgivity.app.model.*
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONException
import java.util.*


class RelatedQuesOptionsAdapter(
    var mActivity: Activity,
    var mData: RelatedQues?,
    var mOptionsList: ArrayList<Options>,
    var onItemClickListener: OnItemClickListener? = null,
    var nugID:String
) : RecyclerView.Adapter<RelatedQuesOptionsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_related_ans_options, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mList : RelatedQues = mData!!
        var mOptions : Options = mOptionsList.get(position)
//        var mList = mData?.options
//        val hashMap = HashMap<String, String>() // Dummy HashMap.
////        var mOptionsList: ArrayList<Options> = ArrayList()
//        val getResultCon = mList // response Any
//        val gson = Gson()
//        val jsonElement = gson.toJsonTree(getResultCon)
//        var mLiiist = gson.fromJson(jsonElement, hashMap::class.java)
////        Log.e("TAG","listt"+mLiiist)
//
//        var index = 0
//        for ((key, value) in mLiiist) {
//            var keyByIndex = mLiiist.keys.elementAt(index) // Get key by index.
//            val valueOfElement = mLiiist.getValue(keyByIndex) // Get value.
//
//            var mModel: Options?=Options()
//            mModel?.option_key = keyByIndex
//            mModel?.option_value = valueOfElement
//            mOptionsList.add(mModel!!)
//            index++
//        }

        holder.txtOptionTV.text=mOptionsList.get(position).option_value
        Log.e("TAG","listt"+mOptionsList.get(position).option_value)




//        Log.e("TAG","listt"+mOptionsList)

        holder.itemView.setOnClickListener {
            mOptions.option_key?.let { it1 ->
                onItemClickListener!!.onItemClickListner(mList.quesId.toString(),
                    it1,nugID)
            }
        }
    }

    override fun getItemCount(): Int {
        return mOptionsList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtOptionTV: TextView = itemView.findViewById(R.id.txtOptionTV)
    }

}
package com.forgivity.app.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.forgivity.app.R
import com.forgivity.app.model.Result
import java.util.*

class ResultAdapter(
    var mActivity: Activity,
    var mOptionsList: ArrayList<Result>,
) : RecyclerView.Adapter<ResultAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_percentage_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val mOptions: Result = mOptionsList.get(position)
        holder.txtAnsTV.text = mOptions.result_key
        holder.txtPecentageTV.text = mOptions.result_value + "%"

    }

    override fun getItemCount(): Int {
        return mOptionsList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtAnsTV: TextView = itemView.findViewById(R.id.txtAnsTV)
        var txtPecentageTV: TextView = itemView.findViewById(R.id.txtPecentageTV)
    }

}
package com.forgivity.app.adapter

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.forgivity.app.R
import com.forgivity.app.interfaces.LoadMoreSavedData
import com.forgivity.app.model.ListItem
import com.forgivity.app.views.activity.AudioActivity

class SavedAdapter(
    var mActivity: Activity, var mArrayList: ArrayList<ListItem?>?,
    var mLoadMoreScrollListener: LoadMoreSavedData? = null
) :
    RecyclerView.Adapter<SavedAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_saved, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mModel: ListItem? = mArrayList!![position]
        holder.txtNuggetTitleTV.text = mModel?.title
        holder.txtNuggetDescriptionTV.text = mModel?.desc
        if (mArrayList!!.size % 10 == 0 && mArrayList!!.size - 1 == position) {
            if (mModel != null) {
                mLoadMoreScrollListener?.onLoadMoreSavedData(mModel)
            }
        }

        holder.itemView.setOnClickListener {
            when (mArrayList?.get(position)?.savedType) {
                "nugget" -> {
                    showPodcastAlertDialog(mActivity, position)
                }
                "podcast" -> {
                    val intent = Intent(mActivity, AudioActivity::class.java)
                    mActivity.startActivity(intent)
                }
                "day" -> {
                    showNuggetAlertDialog(mActivity, position)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return mArrayList?.size!!
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgNuggetIV: ImageView = itemView.findViewById(R.id.imgNuggetIV)
        var txtNuggetTitleTV: TextView = itemView.findViewById(R.id.txtNuggetTitleTV)
        var txtNuggetDescriptionTV: TextView = itemView.findViewById(R.id.txtNuggetDescriptionTV)
    }

    // - - Podcast Alert Dialog
    private fun showPodcastAlertDialog(mActivity: Context?, position: Int) {
        val alertDialog = Dialog(mActivity!! as Context)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_podcast)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val imgCrossIV = alertDialog.findViewById<ImageView>(R.id.imgCrossIV)
        val txtTitleTV = alertDialog.findViewById<TextView>(R.id.txtTitleTV)
        val txtDescriptionTV = alertDialog.findViewById<TextView>(R.id.txtDescriptionTV)
        val txtRelatedListeningTV = alertDialog.findViewById<TextView>(R.id.txtRelatedListeningTV)
        txtRelatedListeningTV.visibility = View.GONE
        txtTitleTV.text = mArrayList?.get(position)!!.title
        txtDescriptionTV.text = mArrayList?.get(position)!!.desc
        imgCrossIV.setOnClickListener {
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    // - - Nugget Dialog
    private fun showNuggetAlertDialog(mActivity: Context?, position: Int) {
        val alertDialog = Dialog(mActivity!! as Context)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_podcast)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val imgCrossIV = alertDialog.findViewById<ImageView>(R.id.imgCrossIV)
        val txtTitleTV = alertDialog.findViewById<TextView>(R.id.txtTitleTV)
        val txtDescriptionTV = alertDialog.findViewById<TextView>(R.id.txtDescriptionTV)
        val txtRelatedListeningTV = alertDialog.findViewById<TextView>(R.id.txtRelatedListeningTV)
        txtTitleTV.text = mArrayList?.get(position)!!.title
        txtDescriptionTV.text = mArrayList?.get(position)!!.desc
        txtRelatedListeningTV.visibility = View.GONE
        imgCrossIV.setOnClickListener {
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    fun filterList(mSavedListFiltered: ArrayList<ListItem?>) {
        this.mArrayList = mSavedListFiltered
        notifyDataSetChanged()
    }
}
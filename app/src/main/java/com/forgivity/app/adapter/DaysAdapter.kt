package com.forgivity.app.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat.*
import androidx.recyclerview.widget.RecyclerView
import com.forgivity.app.R
import com.forgivity.app.interfaces.DaysInterface
import com.forgivity.app.model.Result
import com.forgivity.app.views.activity.AudioActivity
import com.forgivity.app.views.activity.MyProgramActivity
import com.forgivity.app.views.activity.ReviewActivity
import java.util.*

class DaysAdapter(
    var mActivity: Activity,
    var mDaysList: ArrayList<String>,
    var mLetterList: ArrayList<String>,
    var listPos:Int,
    var mDaysInterface: DaysInterface
) : RecyclerView.Adapter<DaysAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_days_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        val mOptions: String = mDaysList.get(position)
        holder.txtDayNumTV.text = mOptions
        holder.txtLevelTV.text = mLetterList.get(position)

        if (listPos == position) {
            holder.txtDayNumTV.setBackgroundResource(R.drawable.day_bg)
            holder.txtDayNumTV.setTextColor(mActivity.getResources().getColor(R.color.colorWhite))
        } else {
            holder.txtDayNumTV.setBackgroundResource(R.drawable.day_bg_white)
            holder.txtDayNumTV.setTextColor(mActivity.getResources().getColor(R.color.colorBlack))
        }

        holder.itemView.setOnClickListener {
            if (listPos >= 0)
                notifyItemChanged(listPos)
            listPos = holder.getAdapterPosition()
            notifyItemChanged(listPos)
            }
        mDaysInterface.onDaysClickListener(mDaysList.get(position))
        }

    override fun getItemCount(): Int {
        return mDaysList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtDayNumTV: TextView = itemView.findViewById(R.id.txtDayNumTV)
        var txtLevelTV: TextView = itemView.findViewById(R.id.txtLevelTV)
    }

}
package com.forgivity.app.adapter

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.forgivity.app.R
import com.forgivity.app.interfaces.OnQuizItemClickInterface
import com.forgivity.app.model.Options
import java.util.*

class RelatedQuizAdapter(
    var mActivity: Activity,
    var answerDesc: String,
    var correctOption: String,
    var mOptionsList: ArrayList<Options>,
    var onItemClickListener: OnQuizItemClickInterface? = null,
    var nugID: String,
    var quesID: String
) : RecyclerView.Adapter<RelatedQuizAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_related_ans_options, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        var mModel: RelatedQues = mData.get(position)
        Log.e("TAG", "listtttttt" + mOptionsList)
        var mOptions: Options = mOptionsList.get(position)

        holder.txtOptionTV.text = mOptionsList.get(position).option_value
        Log.e("TAG", "listt" + mOptionsList.get(position).option_value)

//        Log.e("TAG","listt"+mOptionsList)

        holder.itemView.setOnClickListener {
            mOptions.option_key?.let { it1 ->
                onItemClickListener!!.onQuizItemClickListner(
                    quesID,
                    it1, nugID, answerDesc, correctOption
                )
            }
        }
    }

    override fun getItemCount(): Int {
        return mOptionsList.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtOptionTV: TextView = itemView.findViewById(R.id.txtOptionTV)
    }

}
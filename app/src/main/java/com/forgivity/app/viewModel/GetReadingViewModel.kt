package com.forgivity.app.viewModel

import android.app.Activity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.forgivity.app.model.StatusMsgModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.views.activity.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GetReadingViewModel : ViewModel() {
    private var mReadStatusModel: MutableLiveData<StatusMsgModel>? = null


    /*
    fun getReadingRequest(
        activity: Activity?,
        access_token:String
    ): MutableLiveData<GetQuestionsModelNew>? {
        mModel = MutableLiveData<GetQuestionsModelNew>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getReadingRequest(access_token).enqueue(object : Callback<GetQuestionsModelNew?> {
            override fun onResponse(
                call: Call<GetQuestionsModelNew?>?,
                response: Response<GetQuestionsModelNew?>
            ) {
                if (response.body() != null) {
                    Log.e("GetReadingViewModel","*****RESPONSE****"+response.body())
                    mModel!!.setValue(response.body())
                } else {
                    Log.e("GetReadingViewModel","******GETTING  0 ERROR******")
                }
            }

            override fun onFailure(call: Call<GetQuestionsModelNew?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
                Log.e("GetReadingViewModel","******ERROR******${t.toString()}")
            }
        })
        return mModel
    }
*/

    fun getReadStatusRequest(
        activity: Activity?,
        access_token:String,
        day_id:String,
        nug_id:String
    ): MutableLiveData<StatusMsgModel>? {
        mReadStatusModel = MutableLiveData<StatusMsgModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.getReadStatusRequest(access_token,day_id,nug_id).enqueue(object : Callback<StatusMsgModel?> {
            override fun onResponse(
                call: Call<StatusMsgModel?>?,
                response: Response<StatusMsgModel?>
            ) {
//                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mReadStatusModel!!.setValue(response.body())
                } else {
//                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<StatusMsgModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mReadStatusModel
    }
}
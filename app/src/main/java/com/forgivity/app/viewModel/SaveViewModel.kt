package com.forgivity.app.viewModel

import android.app.Activity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.forgivity.app.model.SaveModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.views.activity.BaseActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SaveViewModel : ViewModel() {
    private var mModel: MutableLiveData<SaveModel>? = null

    fun saveData(
        activity: Activity?,
        access_token:String,
        type_id:String,
        type:String
    ): MutableLiveData<SaveModel>? {
        mModel = MutableLiveData<SaveModel>()
        val apiInterface: ApiInterface = ApiClient.apiClient!!.create(ApiInterface::class.java)
        apiInterface.saveDataRequest(access_token,type_id,type).enqueue(object : Callback<SaveModel?> {
            override fun onResponse(
                call: Call<SaveModel?>?,
                response: Response<SaveModel?>
            ) {
                BaseActivity().dismissProgressDialog()
                if (response.body() != null) {
                    mModel!!.setValue(response.body())
                } else {
                    BaseActivity().dismissProgressDialog()
                }
            }

            override fun onFailure(call: Call<SaveModel?>?, t: Throwable?) {
                BaseActivity().dismissProgressDialog()
            }
        })
        return mModel
    }
}
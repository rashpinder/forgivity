package com.forgivity.app.interfaces

import com.forgivity.app.model.RelatedPod

interface LoadMorePodcastData {
    public fun onLoadMorePodcastData(mModel : RelatedPod)
}
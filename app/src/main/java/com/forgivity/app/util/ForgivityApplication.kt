package com.forgivity.app.util

import android.app.Activity
import android.app.Application
import android.content.ComponentName
import android.content.Intent
import io.branch.referral.Branch
import android.os.Bundle
import android.widget.Toast


 public final class ForgivityApplication : Application(),Application.ActivityLifecycleCallbacks {
     var start_time: Long = 0
     var mid_time: Long = 0
     var new_start_time: Long = 0
     private var activityReferences = 0
     private var isActivityChangingConfigurations = false

    override fun onCreate() {
        super.onCreate()
//        Toast.makeText(this, "Callback"+"onActivityCreated", Toast.LENGTH_SHORT).show()
        registerActivityLifecycleCallbacks(this)
//        val intent = Intent(applicationContext, AppService::class.java)
//        startService(intent)
        // Branch logging for debugging
        Branch.enableLogging()

        // Branch object initialization
        Branch.getAutoInstance(this)
    }

    override fun onActivityCreated(p0: Activity, p1: Bundle?) {


//        Toast.makeText(this, "Callback"+"onActivityCreated", Toast.LENGTH_SHORT).show()
    }

    override fun onActivityStarted(p0: Activity) {
        if (++activityReferences == 1 && !isActivityChangingConfigurations) {
            // App enters foreground
        }
//        Toast.makeText(this, "Callback"+"started", Toast.LENGTH_SHORT).show()

    }

    override fun onActivityResumed(p0: Activity) {
//        Toast.makeText(this, "Callback"+"resumed", Toast.LENGTH_SHORT).show()

    }

    override fun onActivityPaused(p0: Activity) {
//        Toast.makeText(this, "Callback"+"paused", Toast.LENGTH_SHORT).show()

    }


     override fun startForegroundService(service: Intent?): ComponentName? {
         return super.startForegroundService(service)

     }



     override fun onActivityStopped(p0: Activity) {
         isActivityChangingConfigurations = p0.isChangingConfigurations()
         if (--activityReferences == 0 && !isActivityChangingConfigurations) {
             AppPreference().writeString(this, LEAVE_HINT, "true")
             // App enters background
             mid_time= System.currentTimeMillis()
             AppPreference().readLong(this,STARTT_TIME_SPENT, start_time)
             new_start_time=((mid_time - AppPreference().readLong(this,STARTT_TIME_SPENT,0)))
             AppPreference().writeLong(this,NEW_STARTT_TIME_SPENT,new_start_time)
//             Toast.makeText(this, "Callback"+"stopped", Toast.LENGTH_SHORT).show()
         }

    }

    override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {

    }

    override fun onActivityDestroyed(p0: Activity) {
//        Toast.makeText(this, "Callback"+"destroyed", Toast.LENGTH_SHORT).show()
    }
}
package com.forgivity.app.util

// - - Base Server Url
//const val BASE_URL = "https://www.dharmani.com/Forgivity/Api/"
//const val BASE_URL = "https://www.dharmani.com/Forgivity/v2/Api/"
const val BASE_URL = "https://dharmani.com/Forgivity/v2/Api/"


// - - Constant Values
const val SPLASH_TIME_OUT = 1000L
const val ACCEPT = 1
const val REJECT = 2

// - - Link Constants
const val LINK_TYPE = "link_type"
const val LINK_ABOUT = "link_about"
const val LINK_PP = "link_pp"
const val LINK_TERMS = "link_terms"

// - - Link Urls
const val ABOUT_WEB_LINK = BASE_URL + "webservice/about.html"
const val PP_WEB_LINK = "https://www.forgivity.com/privacy"
const val TERMS_WEB_LINK ="https://www.forgivity.com/terms"
const val CONTACT_WEB_LINK ="https://www.forgivity.com/contact"
const val FAQ_WEB_LINK ="https://www.forgivity.com/about"

// - - Shared Preference Keys
const val ACCESS_TOKEN = "access_token"
const val IS_LOGIN = "is_login"
const val GOT_IT = "got_it"
var NAME = "name"
var ORG_ID = "org_id"
var AGE = "age"
var GENDER = "gender"
var INTEREST = "interest"
var PERSON_TYPE = "person_type"
var PROFILE_IMAGE = "profileImage"
var DEVICE_TOKEN = "device_token"
var SUBMIT_ANS_DONE = "device_token"
var CHECKIN = "checkin"
var LEAVE_HINT = "leave_hint"
var INTRO = "introo"
var START_TIME_SPENT = "start_time_spent"
var STARTT_TIME_SPENT = "startt_time_spent"
var NEW_STARTT_TIME_SPENT = "startt_time_spent"
var END_TIME_SPENT = "end_time_spent"
var END_MIN_SPENT = "end_min_spent"
var TOTAL_TIME_SPENT = "total_time_spent"
var TOTALL_TIME_SPENT = "totall_time_spent"
var  IS_CHECKIN = "isCheckin"
const val PHONE_NUMBER = "phone_number"
const val IS_SUBSCRIPTION = "is_subscriptions"


/*
       *
       * Subscriptions Keys & Details
       * */
//const val MERCHANT_ID: String = "5051951214241690750"
//const val LICENCE_KEY: String = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAlhB5030hMoVbcK7c1502GjQ/qcf9ElPesTID8P2+n5301FUl6XCYdxnDWS+fq6ZXG5OQdBwGOJvZUsQ9vcGWwee70zNGTdWzfWt9Qr4KM3yhGLLi65cq2Lm+Y7NAFxQXcDcjUbub7EFmpX9VxgRlkkP8HHQZZULyhLW8jDlA7Ub/jOS5b/xI4/0vTMJz6dTf+WnEF4fKYk53/IQUtOmKUrXS3sMheWCnhAemYMTYylAWBkB634TWP+S6FflmQvfzDMGz+RauKeLwb2jN7vJrVXwCCEoUgNRvv1Gt+1KOMlfScy9aCnXKIBZKfYU81qVjxPjU31s/BYNvPCKbPjMKPQIDAQAB"
//const val SUBSCRIPTION_PLAN_KEY: String = "com.musist.app_monthly_subscriptions_2839423jh4kj23"

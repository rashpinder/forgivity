package com.forgivity.app.fcm
import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.media.RingtoneManager
import android.net.ConnectivityManager
import android.os.Build
import android.util.Log
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.forgivity.app.R
import com.forgivity.app.model.LinkedDetails
import com.forgivity.app.model.LinkedDetailsModel
import com.forgivity.app.retrofit.ApiClient
import com.forgivity.app.retrofit.ApiInterface
import com.forgivity.app.views.activity.AudiooActivity
import com.forgivity.app.views.activity.BaseActivity
import com.forgivity.app.views.activity.FeedActivity
import com.forgivity.app.views.activity.MyProgramActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class NotificationReciever : FirebaseMessagingService() {
    val TAG: String = "NotificationReciever"
    var org_id= 0
    var notify_id = 0
     var linked_id =0
    var mType = ""
    var mLinkedList: LinkedDetails? = null
    var progressDialog: Dialog? = null

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        if (p0.data.size > 0) {
            try {
                var mTempData = p0.data.toString()
                var mNewData = mTempData.split("=")
                val mJsonObject = JSONObject(mNewData[8])
                Log.e(TAG, "**Notifications Data**" + mJsonObject)
                if (!mJsonObject.isNull("notification_type")) {
                    mType = mJsonObject.getString("notification_type")
                }
                if (!mJsonObject.isNull("org_id")) {
                    org_id = mJsonObject.getInt("org_id")
                }
                if (!mJsonObject.isNull("notify_id")) {
                    notify_id = mJsonObject.getInt("notify_id")
                }
                if (!mJsonObject.isNull("linked_id")) {
                    linked_id = mJsonObject.getInt("linked_id")
                }
            } catch (e: Exception) {
                Log.e(TAG, "onMessageReceived: $e")
            }
            sendNotification()
        }
    }


    private fun sendNotification() {
        var intent: Intent? = null
        if (mType.equals("mass")) {
            intent = Intent(this, MyProgramActivity::class.java)
        } else if (mType.equals("nugget")) {
            intent = Intent(this, MyProgramActivity
            ::class.java)
            intent.putExtra("mType",mType)
            intent.putExtra("linked_id",linked_id)
            intent.putExtra("receiver",true)
        } else if (mType.equals("podcast")) {
            intent = Intent(this, AudiooActivity::class.java)
            intent.putExtra("mType",mType)
            intent.putExtra("linked_id",linked_id)
            intent.putExtra("receiver",true)
        } else if (mType.equals("survey")) {
            intent = Intent(this, MyProgramActivity::class.java)
        }else {
            intent = Intent(this, MyProgramActivity::class.java)
        }

        intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
        val pendingIntent = PendingIntent.getActivity(
            this,
            generateRandomInt(5),
            intent,
            PendingIntent.FLAG_ONE_SHOT)

        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val notificationBuilder =
            NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_notifi)
                .setContentTitle("Forgivity")
                .setAutoCancel(true)
                .setVibrate(longArrayOf(500, 500))
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(
            generateRandomInt(5),
            notificationBuilder.build()
        )

}
    private fun generateRandomInt(i: Int): Int {
        val random = Random()
        return random.nextInt(i)
    }

        // - - Podcast Alert Dialog
        fun showNuggetAlertDialog(mActivity: Any?, title: String, description: String) {
            val alertDialog = Dialog(mActivity!! as Context)
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            alertDialog.setContentView(R.layout.dialog_podcast)
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.setCancelable(false)
            alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            // set the custom dialog components - text, image and button
            val imgCrossIV = alertDialog.findViewById<ImageView>(R.id.imgCrossIV)
            val txtTitleTV = alertDialog.findViewById<TextView>(R.id.txtTitleTV)
            val txtDescriptionTV = alertDialog.findViewById<TextView>(R.id.txtDescriptionTV)
            txtTitleTV.text=title
            txtDescriptionTV.text=description

            imgCrossIV.setOnClickListener {
                alertDialog.dismiss()
            }
            alertDialog.show()
        }




    // - - To Show Progress Dialog
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun showProgressDialog(mActivity: Any?) {
        progressDialog = Dialog(mActivity!! as Context)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)
            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }

    // - - To Hide Progress Dialog
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }

    //  Show Toast Message
    fun showToast(mActivity: Context?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }

    //  To Check Internet Connections
    fun isNetworkAvailable(mContext: Any): Boolean {
        val connectivityManager =
           getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    /*
     *
     * Error Alert Dialog
     * */
    fun showAlertDialog(mActivity: Context?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

    }

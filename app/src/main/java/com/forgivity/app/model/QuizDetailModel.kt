package com.forgivity.app.model

data class QuizDetailModel(
    val message: String,
    val quiz_detail: String,
    val status: Int
)
package com.forgivity.app.model

data class LinkedDetailsModel(
    val linked_details: LinkedDetails,
    val linked_type: String,
    val message: String,
    val status: Int
)
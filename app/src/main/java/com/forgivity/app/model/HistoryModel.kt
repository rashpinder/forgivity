package com.forgivity.app.model

data class HistoryModel(
    val all_levels: AllLevels,
    val message: String,
    val status: Int
)
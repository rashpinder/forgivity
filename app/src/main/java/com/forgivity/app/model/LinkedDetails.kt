package com.forgivity.app.model

data class LinkedDetails(
    val day_creation: String,
    val day_desc: String,
    val day_disable: String,
    val day_id: String,
    val day_level: String,
    val day_title: String,
    val description: String,
    val pod_title: String,
    val related_nuggets: String,
    val related_pod: String,
    val title: String,
    val pod_id: String,
    val pod_type: String,
    val pod_display_title: String,
    val pod_desc: String,
    val pod_creation: String,
    val pod_disable: String,
    val welcome_pod: String,
    val position: String,
    val pod_file: String,
)
package com.forgivity.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class SubmitAnswerModel(

	@field:SerializedName("result")
	val result: @RawValue Any? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable


@Parcelize
data class Result(
	var result_key: String? = null,
	var result_value: String? = null
) : Parcelable

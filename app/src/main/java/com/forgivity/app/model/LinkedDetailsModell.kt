package com.forgivity.app.model

data class LinkedDetailsModell(
    val linked_details: LinkedDetailsX,
    val linked_type: String,
    val message: String,
    val status: Int
)
package com.forgivity.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FeedActivityModel(

	@field:SerializedName("all_data")
	val allData: AllDataFeed? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class AllDataFeed(

	@field:SerializedName("last_page")
	val lastPage: Boolean? = null,

	@field:SerializedName("list")
	val list: ArrayList<ListItemFeed?>? = null
) : Parcelable

@Parcelize
data class ListItemFeed(

	@field:SerializedName("notify_type")
	val notifyType: String? = null,

	@field:SerializedName("read_by")
	val readBy: String? = null,

	@field:SerializedName("org_id")
	val orgId: String? = null,

	@field:SerializedName("type_id")
	val typeId: String? = null,

	@field:SerializedName("creation_date")
	val creationDate: String? = null,

	@field:SerializedName("notified_user")
	val notifiedUser: String? = null,

	@field:SerializedName("linked_id")
	val linkedId: String? = null,

	@field:SerializedName("notify_id")
	val notifyId: String? = null,

	@field:SerializedName("notify_message")
	val notifyMessage: String? = null
) : Parcelable

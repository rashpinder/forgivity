package com.forgivity.app.model

data class LinkedDetailsX(
    val day_creation: String,
    val day_desc: String,
    val day_disable: String,
    val day_id: String,
    val day_level: String,
    val day_title: String,
    val description: String,
    val pod_title: String,
    val related_nuggets: String,
    val related_pod: String,
    val title: String
)
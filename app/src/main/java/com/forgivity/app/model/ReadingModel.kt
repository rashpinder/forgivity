package com.forgivity.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class ReadingModel(

	@field:SerializedName("end_date")
	val endDate: String? = null,

	@field:SerializedName("day_detail")
	val dayDetail: DayDetail? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class RelatedPod(

	@field:SerializedName("pod_file")
	val podFile: String? = null,

	@field:SerializedName("pod_display_title")
	val podDisplayTitle: String? = null,

	@field:SerializedName("pod_creation")
	val podCreation: String? = null,

	@field:SerializedName("pod_disable")
	val podDisable: String? = null,

	@field:SerializedName("welcome_pod")
	val welcomePod: String? = null,

	@field:SerializedName("pod_type")
	val podType: String? = null,

	@field:SerializedName("pod_id")
	val podId: String? = null,

	@field:SerializedName("pod_title")
	val podTitle: String? = null,

	@field:SerializedName("pod_desc")
	val podDesc: String? = "",

	@field:SerializedName("position")
val position: String? = null

): Parcelable

@Parcelize
data class RelatedNuggetsItem(

	@field:SerializedName("related_audio")
	val relatedAudio: String? = null,

	@field:SerializedName("nug_disable")
	val nugDisable: String? = null,

	@field:SerializedName("nug_creation")
	val nugCreation: String? = null,

	@field:SerializedName("related_ques")
	val relatedQues:  @RawValue Any? = null,

	@field:SerializedName("nug_desc")
	val nugDesc: String? = null,

	@field:SerializedName("read_status")
	val readStatus: Int? = null,

	@field:SerializedName("nug_title")
	val nugTitle: String? = null,

	@field:SerializedName("related_quiz")
	val relatedQuiz:  @RawValue Any? = null,

	@field:SerializedName("nug_id")
	val nugId: String? = null,

	@field:SerializedName("nug_save_status")
    var nugSaveStatus: Boolean? = null,

	@field:SerializedName("related_pod")
	val relatedPod: @RawValue Any? = null
): Parcelable

@Parcelize
data class DayDetail(

	@field:SerializedName("day_desc")
	val dayDesc: String? = null,

	@field:SerializedName("day_desc_read")
	val dayDescRead: Int? = null,

	@field:SerializedName("day_no")
	val dayNo: String? = null,

	@field:SerializedName("day_save_status")
	val daySaveStatus: Boolean? = null,

	@field:SerializedName("day_disable")
	val dayDisable: String? = null,

	@field:SerializedName("related_nuggets")
	val relatedNuggets: ArrayList<RelatedNuggetsItem>,

	@field:SerializedName("day_creation")
	val dayCreation: String? = null,

	@field:SerializedName("day_level")
	val dayLevel: String? = null,

	@field:SerializedName("day_id")
	val dayId: String? = null,

	@field:SerializedName("related_pod")
	val relatedPod:  @RawValue Any?,

	@field:SerializedName("letter_drop")
	val letter_drop: LetterDrop? =null
): Parcelable

@Parcelize
data class RelatedQues(

	@field:SerializedName("correct_option")
	val correctOption: String? = null,

	@field:SerializedName("ques_id")
	val quesId: String? = null,

	@field:SerializedName("ques_desc")
	val quesDesc: String? = null,

	@field:SerializedName("ques_type")
	val quesType: String? = null,

	@field:SerializedName("answer_desc")
	val answerDesc: String? = null,

//	@field:SerializedName("options")
//	val options: List<Map<String, String>>,

//	@field:SerializedName("options")
//	val options: List<String?>? = null,

//	@field:SerializedName("options")
//	val options: @RawValue Any? = null,

	@field:SerializedName("options")
	val options: @RawValue Any? = null,

	@field:SerializedName("ques_creation")
	val quesCreation: String? = null,

	@field:SerializedName("position")
	val position: String? = null,

	@field:SerializedName("related_pod")
	val relatedPod: String? = null,

	@field:SerializedName("ques_title")
	val quesTitle: String? = null,

	@field:SerializedName("answer_status")
    var answerStatus: Boolean? = null,

	@field:SerializedName("ques_disable")
	val quesDisable: String? = null
): Parcelable


@Parcelize
data class RelatedQuiz(

	@field:SerializedName("quiz_id")
	val quizId: String? = null,

	@field:SerializedName("quiz_ques")
	val quizQues: String? = null,

	@field:SerializedName("all_questions")
	val questions: List<RelatedQues?>? = null,

	@field:SerializedName("quiz_title")
	val quizTitle: String? = null,

	@field:SerializedName("creation_date")
	val creationDate: String? = null,

	@field:SerializedName("day_level")
	val dayLevel: String? = null
) : Parcelable

@Parcelize
data class LetterDrop(

	@field:SerializedName("pod_creation")
	val pod_creation: String? = null,

	@field:SerializedName("pod_desc")
	val pod_desc: String? = null,

	@field:SerializedName("pod_disable")
	val pod_disable: String? = null,

	@field:SerializedName("pod_display_title")
	val pod_display_title: String? = null,

	@field:SerializedName("pod_file")
	val pod_file: String? = null,

	@field:SerializedName("pod_id")
	val pod_id: String? = null,

	@field:SerializedName("pod_title")
	val pod_title: String? = null,

	@field:SerializedName("pod_type")
	val pod_type: String? = null,

	@field:SerializedName("position")
	val position: String? = null,

	@field:SerializedName("welcome_pod")
	val welcome_pod: String? = null,

) : Parcelable


//
//@Parcelize
//data class AllQuestionsItem(
//
//	@field:SerializedName("correct_option")
//	val correctOption: String? = null,
//
//	@field:SerializedName("ques_id")
//	val quesId: String? = null,
//
//	@field:SerializedName("ques_desc")
//	val quesDesc: String? = null,
//
//	@field:SerializedName("ques_type")
//	val quesType: String? = null,
//
//	@field:SerializedName("answer_desc")
//	val answerDesc: String? = null,
//
//	@field:SerializedName("options")
//	val options: Options? = null,
//
//	@field:SerializedName("ques_creation")
//	val quesCreation: String? = null,
//
//	@field:SerializedName("position")
//	val position: String? = null,
//
//	@field:SerializedName("related_pod")
//	val relatedPod: String? = null,
//
//	@field:SerializedName("ques_title")
//	val quesTitle: String? = null,
//
//	@field:SerializedName("answer_status")
//	val answerStatus: Boolean? = null,
//
//	@field:SerializedName("ques_disable")
//	val quesDisable: String? = null
//) : Parcelable


//
//@Parcelize
//data class RelatedQuiz(
//
//	@field:SerializedName("title")
//	val title: String? = null,
//
//	@field:SerializedName("id")
//	val id: String? = null,
//
//	@field:SerializedName("questions")
//	val questions:  @RawValue Any? = null,
//): Parcelable


@Parcelize
data class Options(
	var option_key: String? = null,
	var option_value: String? = null,
) : Parcelable


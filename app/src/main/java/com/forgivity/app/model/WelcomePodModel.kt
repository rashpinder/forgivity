package com.forgivity.app.model

data class WelcomePodModel(
    val message: String,
    val pod_detail: RelatedPod,
    val status: Int
)
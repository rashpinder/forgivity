package com.forgivity.app.model

data class AllLevels(
    val F: List<String>,
    val O: List<String>,
    val R: List<String>,
    val G: List<String>,
    val I: List<String>,
    val V: List<String>,
    val E: List<String>
)
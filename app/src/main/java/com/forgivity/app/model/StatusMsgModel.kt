package com.forgivity.app.model

data class StatusMsgModel(
    val message: String,
    val status: Int
)